/// <summary>
/// Codeunit de funciones
/// </summary>
codeunit 50104 "DTAFuncionesAppC01"
{
    //aqui pondremos como funciones las suscripciones de la codeunit suscripciones que ocupen mas de una linea de codigo
    /// <summary>
    /// Funcion ejemplo ambito parametros y variables
    /// </summary>
    /// <param name="pCodCliente">Code[20].</param>
    /// <param name="pParam2">Integer.</param>
    /// <param name="prCustomer">VAR Record Customer.</param>
    /// <returns>Return variable return of type Text.</returns>
    procedure FuncionEjemplo01F(pCodCliente: Code[20]; pParam2: Integer; var prCustomer: Record Customer) return: Text
    var

    begin

    end;

    /// <summary>
    /// Funcion ejemplo ambito variables como parametros
    /// </summary>
    /// <param name="pTexto">VAR Text.</param>
    procedure EjemploVariablesESF(var pTexto: Text)
    begin
        ptexto := 'tecon servicios';
    end;

    /// <summary>
    /// Función de ejemplo ambito parametro tipo textbuilder
    /// </summary>
    /// <param name="pTexto">TextBuilder.</param>
    procedure EjemploVariablesES02F(pTexto: TextBuilder)
    begin
        ptexto.append('tecon servicios');
    end;

    /// <summary>
    /// Función ejemplo array con juego del Bingo
    /// </summary>
    procedure EjemploArrayF()
    var
        mlNumeros: array[20] of Text;
        i: Integer;
        xlNumElementosArray: Integer;
        xlRamdomObtenido: Integer;
    begin
        randomize();
        for i := 1 to ArrayLen(mlNumeros) do begin
            mlNumeros[i] := format(random(ArrayLen(mlNumeros)));
        end;
        xlNumElementosArray := CompressArray(mlNumeros);
        Message('la bola que ha salido es %1');
        Message(mlNumeros[xlRamdomObtenido]);
        mlNumeros[xlRamdomObtenido] := '';
    end;

    /// <summary>
    /// Ejemplo de Bingo
    /// </summary>
    procedure BingoF()
    var
        mlNumeros: array[7] of Text;
        i: Integer;
        //xlNumElementosArray: Integer;
        xlRamdomObtenido: Integer;
    begin
        randomize();
        for i := 1 to ArrayLen(mlNumeros) do begin

            mlNumeros[i] := format(i);
        end;
        repeat
            xlRamdomObtenido := random(CompressArray(mlNumeros));
            Message('la bola que ha salido es %1', mlNumeros[xlRamdomObtenido]);
            mlNumeros[xlRamdomObtenido] := '';
        until CompressArray(mlNumeros) <= 0;
        // Message(mlNumeros[xlRamdomObtenido]);
        // mlNumeros[xlRamdomObtenido] := '';
    end;

    /// <summary>
    /// Función que devuelve el numero de la ultima linea de la tabla de ventas
    /// </summary>
    /// <param name="pRec">record "Sales Line".</param>
    /// <returns>Return value of type Integer.</returns>
    procedure UltimoNumLineaF(pRec: record "Sales Line"): Integer
    var
        rlRec: Record "Sales Line";
    begin
        rlrec.SetCurrentKey("Document Type", "Document No.", "Line No.");
        rlRec.SetRange("Document Type", pRec."Document Type");
        rlRec.SetRange("Document No.", pRec."Document No.");
        if rlRec.FindLast() then begin
            exit(rlrec."Line No.");
        end;
    end;

    /// <summary>
    /// Función que devuelve el numero de la ultima linea de la tabla de compras
    /// </summary>
    /// <param name="pRec">record "Purchase Line".</param>
    /// <returns>Return value of type Integer.</returns>
    procedure UltimoNumLineaF(pRec: record "Purchase Line"): Integer
    var
        rlRec: Record "Purchase Line";
    begin
        rlrec.SetCurrentKey("Document Type", "Document No.", "Line No.");
        rlRec.SetRange("Document Type", pRec."Document Type");
        rlRec.SetRange("Document No.", pRec."Document No.");
        if rlRec.FindLast() then begin
            exit(rlrec."Line No.");
        end;
    end;
    /// <summary>
    /// Función que devuelve el numero de la ultima linea de la tabla de diario general ventas
    /// </summary>
    /// <param name="pRec">record "Gen. Journal Line".</param>
    /// <returns>Return value of type Integer.</returns>
    procedure UltimoNumLineaF(pRec: record "Gen. Journal Line"): Integer
    var
        rlRec: Record "Gen. Journal Line";
    begin
        rlrec.SetCurrentKey("Journal Template Name", "Journal Batch Name", "Line No.");
        rlRec.SetRange("Journal Batch Name", pRec."Journal Batch Name");
        rlRec.SetRange("Line No.", pRec."Line No.");
        if rlRec.FindLast() then begin
            exit(rlrec."Line No.");
        end;
    end;
    /// <summary>
    /// funcion ejemplo de sobrecarga
    /// </summary>
    /// <param name="p1">Integer.</param>
    /// <param name="p2">Decimal.</param>
    /// <param name="p3">text.</param>
    procedure EjemploSobrecargaF(p1: Integer; p2: Decimal; p3: text)
    begin
        EjemploSobrecargaF(p1, p2, p3, false);
    end;
    /// <summary>
    /// funcion ejemplo de sobrecarga 2
    /// </summary>
    /// <param name="p1">Integer.</param>
    /// <param name="p2">Decimal.</param>
    /// <param name="p3">text.</param>
    /// <param name="pBooleano">Boolean.</param>
    procedure EjemploSobrecargaF(p1: Integer; p2: Decimal; p3: text; pBooleano: Boolean)
    var
        xlNuevoNumero: Decimal;
    begin
        p1 := 1;
        p2 := 12.3;
        p3 := 'lo que sea';
        //nueva funcionalidad
        if pBooleano then begin
            xlNuevoNumero := p1 * p2;
        end;
    end;


    /// <summary>
    /// funcion ejemplo de sobrecarga 3
    /// </summary>
    procedure UsoFuncionEjemploF()
    begin
        EjemploSobrecargaF(10, 34.56, 'hola mundo');
    end;

    /// <summary>
    /// funcion ejemplo sintaxis
    /// </summary>
    procedure Sintaxis()
    var
        i: Integer;
        xlTexto: Text;
        z: Decimal;
        x: Integer;
        xbool: Boolean;
        rlcustomer: Record Customer;
        clSeparador: Label '·', Locked = true;
        Ventana: Dialog;
        xlLong: Integer;
        xlPos: Integer;
        xlcampos: List of [Text];
        xlcampo: Text;
        xlFecha: Date;
    begin
        i := 1;
        i := i + 10;
        i += 10;
        xlTexto := 'David';
        xlTexto := xlTexto + 'Tornero';
        xlTexto += 'Alcazar';
        //operadores
        i := x + z;
        i := 9 div 2;
        i := 9 mod 2;
        i := abs(-9);
        xbool := not true;
        xbool := false and true;
        xbool := false or true;
        // operadores relacionales
        xbool := 1 > 2;
        xbool := 1 in [1, 'b', 3, 'i'];
        xbool := 'a' in ['a' .. 'z'];
        //funciones muy usadas
        Message('proceso finalizado');
        Message('el cliente %1 no tiene saldo de pago', rlcustomer.name);
        Error('proceso cancelado por el usuario');
        Error('proceso cancelado por el usuario %1', UserId);
        Error(''); //para la ejecucion y hace rollback, si hay que guardar datos antes del rollback poner la instrucion commit();
        xbool := Confirm('confirma que desea registrar la factura %1', false, 'fa343223');
        i := StrMenu('enviar,facturar,enviar y facturar', 3, '¿como desea registrar?');
        case i of
            0:
                Error('proceso cancelado');
            1:
                ;//codigo enviar
            2:
                ;//codigo facturar
            3:
                ;//codigo enviar y facturar
        end;

        if GuiAllowed then begin
            Ventana.Open('Procesando bucle i #1##########\' +
                         'Procesando bucle x #2##########');
        end;
        for i := 1 to 1000 do begin
            if GuiAllowed then begin
                Ventana.Update(1, i);
            end;
            for x := 1 to 1000 do begin
                if GuiAllowed then begin
                    Ventana.Update(2, x);
                end;
            end;
        end;
        if GuiAllowed then begin
            Ventana.Close();
        end;

        //funciones cadenas de texto

        xlLong := MaxStrLen(xlTexto); //longitud maxima que puede almacenar
        xlLong := MaxStrLen('abc');   // devolvera un 3
        xlTexto := CopyStr('abc', 2, 1); // copia de una cadena otra cadena. aqui retornaria b
        xlTexto := xltexto.Substring(2); // igual que copystr
        xlPos := strpos('Elefante', 'e'); // devuelve la posicion de un cadena dentro de otra .devolveria un 3 porque distingue mayusculas y minusculas
        xlLong := StrLen('abcd'); // devuelve el tamaño de la cadena. aqui seria 4

        // ponen texto en mayusculas o minusculas
        xlTexto := UpperCase(xlTexto); // en desuso solo para versiones antiguas de nav que usen lebguaje c/al
        xltexto := xlTexto.ToUpper();
        xlTexto := LowerCase(xlTexto); // en desuso solo para versiones antiguas de nav que usen lebguaje c/al
        xltexto := xlTexto.ToLower();
        xlTexto := '123,456,789';
        Message(SelectStr(2, xlTexto)); // devuelve 456. solo sirve si valores separados por comas
        message(xlTexto.Split('·').Get(2)); // devuelve 456 tb. Asi es como debemos hacerlo
        //lo mismo pero creando una variable de tipo lista de texto
        xlTexto := '123·456·tecon servicios · 789';
        xlcampos := xlTexto.Split(clSeparador);
        Message(xlcampos.Get(2)); // devuelve 456 tb
        //bucle estatico
        foreach xltexto in xlcampos do begin

        end;
        //bucle para que lo haga dinamicamente
        foreach xlcampo in xlTexto.Split(clSeparador) do begin

        end;

        //empezamos a trabajar con ficheros
        xlTexto := '21-05-2022';
        Evaluate(xlFecha, convertstr(xlTexto, '-', '/')); // convierte los caracteres - a /
        Evaluate(xlFecha, xlTexto.Replace('-', '/')); //igual que la anterior pero de otra forma

        xlTexto := 'Elefante';
        Message(DelChr(xlTexto, '<', 'e')); // elimina la e del principio. con > el final, = en medio , Se pueden combinar <= etc

        //potencias
        xlPos := Power(3, 2); // serie 3 elevado a 2 devolveria 9

        xltexto := UserId(); // devuelve el usuario que tiene iniciada sesion en BC
        xlTexto := CompanyName(); // devuelve el nombre de la empresa en la que estoy logueado
        Today; // devuelve el dia actual
        WorkDate(); // devuelve la fecha de trabajo
        Time; // devuelve la hora actual

        // blob,file manejo de ficheros in stream y out stream







    end;

    /// <summary>
    /// devuelve los caracteres de una cadena de texto a partir del principio de la cadena que le pasamos
    /// </summary>
    /// <param name="pCadena">text.</param>
    /// <param name="pNumCaracteres">Integer.</param>
    /// <returns>Return value of type text.</returns>
    procedure LeftF(pCadena: text; pNumCaracteres: Integer): text

    begin
        if pNumCaracteres > strlen(pCadena) then begin
            Error('N1 de caracteres superior a la logintud de la cadeba');
        end;
        pCadena := CopyStr(pCadena, 1, pNumCaracteres);
        exit(pCadena);
    end;

    /// <summary>
    /// devuelve los caracteres de una cadena de texto a partir del final de la cadena que le pasamos
    /// </summary>
    /// <param name="pCadena">text.</param>
    /// <param name="pNumCaracteres">Integer.</param>
    /// <returns>Return value of type text.</returns>

    procedure RightF(pCadena: text; pNumCaracteres: Integer): text

    begin
        // aqui vendria el if igual que antes
        exit(CopyStr(pcadena, (StrLen(pcadena) - pNumCaracteres) + 1));

    end;

    procedure StreamsF()
    var
        TempLDTAConfiguracionC01: Record DTAConfiguracionC01 temporary;
        xlInStr: InStream;
        xlOutStr: OutStream;


        xlLinea: Text;
        xlNombreFichero: Text;
    begin
        TempLDTAConfiguracionC01.MiBlob.CreateOutStream(xlOutStr); // creo conexion out stream a ese campo miblob de la tabla
        xlOutStr.WriteText(('lo que sea')); // meto la informacion
        TempLDTAConfiguracionC01.MiBlob.CreateInStream(xlInStr); // creo conexion in stream a ese campo miblob
        xlInStr.ReadText(xlLinea); // leo informacion de ese campo
        // para descargar el fichero a nuestro pc // lo descargara siempre en la carpeta Descargas
        xlNombreFichero := 'Pedido.csv';
        if not DownloadFromStream(xlInStr, '', '', '', xlNombreFichero) then begin
            Error('No se ha podido descargar el fichero');
        end;
    end;

    local procedure UsoDeListasF()
    var
        xlLista: list of [code[20]];
    begin

    end;

    // procedure DameCliente10000F():Text
    // var
    //  rlCustomer: Record Customer;
    //  begin
    //      if rlCustomer.get('10000') then begin

    //      end;
    //      exit(rlCustomer.Name);

    //  end;
    procedure getlinea()
    var
        rlcustomer: record customer;
        rlcustomer2: record customer;
        rlSalesLines: Record "Sales Line";
        rlDimensionSetEntry: record "Dimension set entry";
        rlCompanyInformation: record "company information";
    begin
        rlSalesLines.get("sales document type"::Order, '101005', '10000');
        rlDimensionSetEntry.Get(7, 'GRUPONEGOCIO');
        rlCompanyInformation.get(); // las tablas de registro unico tiene un primary key vacio siempre

        rlcustomer.SetFilter(name, '%1|%2', '*A*', '*B*');
        if rlcustomer.FindSet(true, true) then begin
            repeat
                rlcustomer2 := rlcustomer;
                rlcustomer2.Find(); // no tenemos que preocuparnos de buscar claves principales como con el get que seria la otra forma de hacerlo
                                    //poner aqui codigo las modificaciones que queramos

            until rlcustomer.Next() = 0;
        end;
        rlCustomer.find(); // find si que respeta filtros. 
    end;

    local procedure OtrasfuncionesF()
    var
        rlSalesHeader: Record "Sales Header";
        rlSalesInvoiceHeader: record "sales invoice header";
    begin
        rlSalesInvoiceHeader.TransferFields(rlSalesHeader);
    end;

    procedure AbrirListaClientesF(pNotificacion: Notification)
    var
        rlCustomer: Record Customer;
        clNombreDato: Label 'CodigoCliente';
    begin
        if pNotificacion.HasData(clNombreDato) then begin
            rlCustomer.SetRange("No.", pNotificacion.GetData(clNombreDato));
        end;
        page.Run(page::"Customer List", rlCustomer);
    end;

    procedure CodeunitSalesPostOnAfterPostSalesDocF(SalesHeader: Record "Sales Header")
    var
        rlCustomer: record Customer;
        rlSalesLine: Record "Sales Line";
        xlNotificacion: Notification;
        clMsgImporteBajo: Label 'la linea %1 del documento %2 numero %3, con importe %4 es inferior a 100';
    begin
        if rlCustomer.get(SalesHeader."Sell-to Customer No.") then begin
            //compruebo que cliente tenga vacuna
            if rlCustomer.DTATipoVacunaC01 = '' then begin
                xlNotificacion.Id(CreateGuid()); // si mandamos varias notificaciones hay que ponerle id
                xlNotificacion.Message('El cliente no tiene ninguna vacuna');
                xlNotificacion.AddAction('Abrir ficha de cliente', codeunit::DTAFuncionesAppC01, 'AbrirFichaClienteF');
                xlNotificacion.SetData('CodigoCliente', rlCustomer."No.");
                xlNotificacion.Send();
            end;
            //compruebo importes de linea por debajo de 100
            rlSalesLine.SetRange("Document Type", SalesHeader."Document Type");
            rlSalesLine.SetRange("Document No.", SalesHeader."No.");
            rlSalesLine.SetFilter(Amount, '<%1', 100);
            rlSalesLine.SetLoadFields("Line No.", "Document No.", Amount);
            if rlSalesLine.FindSet(false) then begin
                repeat
                    Clear(xlNotificacion);
                    xlNotificacion.Id(CreateGuid());
                    xlNotificacion.Message(StrSubstNo(clMsgImporteBajo, rlSalesLine."Line No.", SalesHeader."Document Type", rlSalesLine."Document No.", rlSalesLine.Amount));
                    xlNotificacion.Send();
                until rlSalesLine.Next() = 0;

            end;

        end;


    end;

    procedure AbrirFichaClienteF(pNotificacion: Notification)
    var
        rlCustomer: Record Customer;
        clNombreDato: Label 'CodigoCliente';
    begin
        if pNotificacion.HasData(clNombreDato) then begin
            if rlCustomer.get(pNotificacion.GetData(clNombreDato)) then begin
                page.Run(page::"Customer Card", rlCustomer);
            end;
        end;
    end;
}