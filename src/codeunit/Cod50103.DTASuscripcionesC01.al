/// <summary>
/// Codeunit de suscripciones
/// </summary>
codeunit 50103 "DTASuscripcionesC01"
{
    SingleInstance = true;

    [EventSubscriber(ObjectType::Codeunit, codeunit::"Sales-Post", 'OnBeforePostSalesDoc', '', false, false)]
    local procedure CodeunitSalesPostOnBeforePostSalesDocF(var SalesHeader: Record "Sales Header")
    begin
        SalesHeader.TestField("External Document No.");//verificamos que ese campo tenga algo
    end;

    [EventSubscriber(ObjectType::Table, Database::"record link", 'OnaftervalidateEvent', 'URL1', false, false)]
    local procedure DatabaserecordlinkOnaftervalidateEventURL1F(CurrFieldNo: Integer; var Rec: Record "Record Link"; var xRec: Record "Record Link")
    begin

    end;

    [EventSubscriber(ObjectType::Codeunit, codeunit::"Purch.-Post", 'OnAfterPostPurchaseDoc', '', false, false)]
    local procedure CodeunitPurchPostOnAfterPostPurchaseDocF(PurchInvHdrNo: Code[20])
    begin
        Message('la factura a sido creada por %1 con el numero de factura %2', UserId, PurchInvHdrNo);
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnAfterPostSalesDoc', '', false, false)]
    local procedure CodeunitSalesPostOnAfterPostSalesDocF(var SalesHeader: Record "Sales Header")
    var
        culDTAFuncionesAppC01: Codeunit DTAFuncionesAppC01;
    begin
        culDTAFuncionesAppC01.CodeunitSalesPostOnAfterPostSalesDocF(SalesHeader);
    end;

    var
#pragma warning disable AA0137
        culDTAFuncionesAppC01: Codeunit DTAFuncionesAppC01;
#pragma warning restore AA0137
}

