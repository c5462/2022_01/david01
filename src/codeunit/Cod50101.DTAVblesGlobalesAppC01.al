/// <summary>
/// Codeunit de variables globales
/// </summary>
codeunit 50101 "DTAVblesGlobalesAppC01"
{
    SingleInstance = true; // se mantiene hasta que se cierra sesion
    /// <summary>
    /// funcion set para darle valor a un parametro
    /// </summary>
    /// <param name="pValor">Boolean.</param>
    procedure SetSwitchF(pValor: Boolean)
    begin
        xSwitch := pvalor;

    end;

    /// <summary>
    /// Funcion get para traer valor dado a un parametro
    /// </summary>
    /// <returns>Return value of type Boolean.</returns>
    procedure GetSwitchF(): Boolean
    begin
        exit(xSwitch)
    end;
    /// <summary>
    /// Guarda el nombre a nivel global BC
    /// </summary>
    /// <param name="pName">Text[100].</param>
    procedure NombreF(pName: Text[100])
    begin
        xName := pName;

    end;
    /// <summary>
    /// Devuelve el nombre guardado globalmente
    /// </summary>
    /// <returns>Return value of type Text.</returns>
    procedure NombreF(): Text
    begin
        exit(xName);
    end;

    procedure ImportandoPedidosF(pImportando: Boolean)
    begin
        XImportantoPedidos := pImportando;
    end;

    var
        xSwitch: Boolean;

    var
        xName: Text;
        XImportantoPedidos: Boolean;
}