/// <summary>
/// Codeunit de Captura de Errores
/// </summary>
codeunit 50102 "DTACapturarErroresC01"
{
    trigger OnRun()
    var
        rlCustomer: Record Customer;
    begin
        rlCustomer.get('AAA');
        Message('datos del cliente %1', rlCustomer);
    end;


    /// <summary>
    /// Funcion para capturar errores, se le pasa un parametro y devuelve un boolenao
    /// </summary>
    /// <param name="pCodProveedor">Code[20].</param>
    /// <returns>Return value of type Boolean.</returns>
    procedure FuncionParaCapturarErrorF(pCodProveedor: Code[20]): Boolean

    var
        rlVendor: Record Vendor;
    begin
        if not rlVendor.get(pCodProveedor) then begin
            rlVendor.Init();
            rlvendor.Validate("No.", pCodProveedor);
            rlVendor.Insert(true); // poner aqui comentario de por que true o false en el insert
            if AsignarDatosProveedorF(rlVendor) then begin
                rlVendor.Modify(true);
            end else begin
                Message('Error al asignar datos (%1', GetLastErrorText());
            end;
            exit(true);
        end;
        Message('ya existe el proveedor %1', pCodProveedor);
    end;

    [TryFunction]
    local procedure AsignarDatosProveedorF(var rlVendor: Record Vendor)
    begin
        rlVendor.Validate("name", 'Proveedor Prueba');
        rlVendor.Validate(Address, 'calle prueba');
        rlVendor.Validate("Payment Method Code", 'JJ');
    end;

    /// <summary>
    /// Funcion para capturar errores en el proceso de importacion de pedidos de venta
    /// </summary>
    /// <param name="prSalesHeader">VAR Record "Sales Header".</param>
    /// <param name="pPosicion">Integer.</param>
    /// <param name="pSeparador">Text.</param>
    /// <param name="pLinea">Text.</param>
    [TryFunction]
    procedure AsignaCabeceraPedidoF(var prSalesHeader: Record "Sales Header"; pPosicion: Integer; pSeparador: Text; pLinea: Text)
    var
        xlFEcha: date;
        xlEstado: Enum "Sales Document Status";
    begin

        case pPosicion of
            4:
                begin
                    prSalesHeader.Validate("Sell-to Customer No.", pLinea.Split(pSeparador).Get(pPosicion));
                end;
            5:
                begin
                    prSalesHeader.Validate("Sell-to Customer Name", pLinea.Split(pSeparador).Get(5));
                end;
            6:
                begin
                    prSalesHeader.Validate("External Document No.", pLinea.Split(pSeparador).Get(6));
                end;
            7:
                begin
                    prSalesHeader.Validate("Location Code", pLinea.Split(pSeparador).Get(7));
                end;
            8:
                begin
                    if Evaluate(xlFEcha, pLinea.Split(pSeparador).Get(8)) then begin
                        prSalesHeader.Validate("Posting Date", xlFEcha);
                    end;
                end;
            9:
                begin
                    if Evaluate(xlEstado, pLinea.Split(pSeparador).Get(9)) then begin
                        prSalesHeader.Validate(Status, xlEstado);
                    end;
                end;

        end;

    end;


    /// <summary>
    /// Funcion para capturar errores en el proceso de importacion de las lineas de pedidos de venta
    /// </summary>
    /// <param name="prSalesLine">VAR Record "Sales Line".</param>
    /// <param name="pPosicion">Integer.</param>
    /// <param name="pSeparador">Text.</param>
    /// <param name="pLinea">Text.</param>
    [TryFunction]
    procedure AsignaLineasPedidoF(var prSalesLine: Record "Sales Line"; pPosicion: Integer; pSeparador: Text; pLinea: Text)
    var
        xlTipoLinea: Enum "Sales Line Type";
        xlInt: Integer;
    begin

        case pPosicion of
            5:
                begin
                    if Evaluate(xlTipoLinea, pLinea.Split(pSeparador).Get(5)) then begin
                        prSalesLine.Validate(Type, xlTipoLinea);
                    end;
                end;
            6:
                begin
                    prSalesLine.Validate("No.", pLinea.Split(pSeparador).Get(6));
                end;
            7:
                begin
                    prSalesLine.Validate("Location Code", pLinea.Split(pSeparador).Get(7));
                end;
            8:
                begin
                    if evaluate(xlInt, pLinea.Split(pSeparador).Get(8)) then begin
                        prSalesLine.Validate(Quantity, xlInt);
                    end;
                end;
            9:
                begin
                    if evaluate(xlInt, pLinea.Split(pSeparador).Get(9)) then begin
                        prSalesLine.Validate("Unit Price", xlInt);
                    end;
                end;
            10:
                begin
                    if evaluate(xlInt, pLinea.Split(pSeparador).Get(10)) then begin
                        prSalesLine.Validate(amount, xlInt);
                    end;
                end;

        end;

    end;


}
