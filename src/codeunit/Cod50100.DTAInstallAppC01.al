/// <summary>
/// Codeunit de instalación
/// </summary>
codeunit 50100 "DTAInstallAppC01"
{
    Subtype = Install; // se ejecuta automaticamente cuando se instala nuestra App
    trigger OnInstallAppPerCompany() // se ejecuta el trigger tantas veces como empresas tengamos
    begin

    end;

    trigger OnInstallAppPerDatabase()// se ejecuta el trigger solo una vez

    begin
    end;

}
