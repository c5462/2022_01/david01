/// <summary>
/// Codeunit para retrasar ejecuciones
/// </summary>
codeunit 50105 "DTACodeunitLentaC01"
{
    //para realizar un proceso que tardara mucho
    trigger OnRun()
    begin
        EjecutarTareaF();
    end;
    /// <summary>
    /// funcion para retrasar ejecucion de tarea
    /// </summary>
    local procedure EjecutarTareaF()
    var
        xlEjecutar: Boolean;
    begin
        xlEjecutar := true;
        if GuiAllowed then begin
            xlEjecutar := Confirm('¿desea ejecutar esta tarea que tarda mucho?');
        end else begin
            InsertarLogF('Tarea Iniciada');
            Commit(); // grabame dice OJO esto no usarlo casi nunca
            Sleep(5000);
            InsertarLogF('Tarea Finalizada');
        end;
    end;
    /// <summary>
    /// funcion para insertar en tabla dtalog
    /// </summary>
    /// <param name="pMensaje">Text.</param>
    local procedure InsertarLogF(pMensaje: Text)
    var
        rlDTALogC01: Record DTALogC01;
    begin
        rlDTALogC01.Init();
        rlDTALogC01.Insert(true);
        rlDTALogC01.Validate(Mensaje, pMensaje);
        rlDTALogC01.Modify(true);
    end;
}