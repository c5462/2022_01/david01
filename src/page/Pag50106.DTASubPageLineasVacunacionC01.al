page 50106 "DTASubPageLineasVacunacionC01"
{
    Caption = 'SubPágina Lineas de Vacunación';
    PageType = ListPart;
    SourceTable = DTALineasPlanVacunacionC01;
    ApplicationArea = all;
    UsageCategory = Lists;
    AutoSplitKey = true;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Codigo; Rec.CodigoLineas)
                {
                    ToolTip = 'Specifies the value of the Código field.';
                    ApplicationArea = All;
                    Editable = false;
                    Visible = false;
                }
                field(NumLinea; Rec.NumLinea)
                {
                    ToolTip = 'Specifies the value of the Nº de Línea field.';
                    ApplicationArea = All;
                    editable = false;
                    Visible = false;
                }
                field(ClienteAVacunar; Rec.ClienteAVacunar)
                {
                    ToolTip = 'Specifies the value of the Cliente a Vacunar field.';
                    ApplicationArea = All;
                }
                field(NombreCliente; rec.NombreclienteF())
                {
                    ApplicationArea = all;
                    Caption = 'Nombre cliente a vacunar';

                }

                field(FechaVacunacion; Rec.FechaVacunacion)
                {
                    ToolTip = 'Specifies the value of the Fecha de  Vacunación field.';
                    ApplicationArea = All;
                }
                field(TipoVacuna; Rec.CodigoVacuna)
                {
                    ToolTip = 'Specifies the value of the Tipo Vacuna field.';
                    ApplicationArea = All;
                }
                field(DesVacuna; rec.DescripcionVariosF(eDTATipoC01::vacuna, Rec.CodigoVacuna))
                {
                    ApplicationArea = all;
                    Caption = 'Descripción  de tipo vacuna';

                }
                field(TipoOtros; Rec.CodigoOtros)
                {
                    ToolTip = 'Specifies the value of the Tipo otros field.';
                    ApplicationArea = All;
                }
                field(DescOtros; rec.DescripcionVariosF(eDTATipoC01::otros, Rec.CodigoOtros))
                {
                    ApplicationArea = all;
                    Caption = 'Descripcion de tipo otros';

                }
                field(FechaProximaVacunacion; Rec.FechaProximaVacunacion)
                {
                    ToolTip = 'Specifies the value of the Fecha proxima vacunacion field.';
                    ApplicationArea = All;
                }
            }
        }
    }
    // mejor poner trigger en la tabla en el campo fecha vacunacion
    // trigger OnNewRecord(BelowxRec: Boolean)
    // var
    //     rlDTACabeceraPlanVacunacionC01: Record DTACabeceraPlanVacunacionC01;
    // begin
    //     if rlDTACabeceraPlanVacunacionC01.Get(rec.CodigoLineas) then begin
    //         rec.Validate(FechaVacunacion, rlDTACabeceraPlanVacunacionC01.FechaInicioVacunacion);
    //     end;

    // end;

    var

        eDTATipoC01: Enum DTATipoC01;
}
