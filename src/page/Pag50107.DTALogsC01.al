page 50107 "DTALogsC01"
{
    ApplicationArea = All;
    Caption = 'Logs';
    PageType = List;
    SourceTable = DTALogC01;
    UsageCategory = Lists;
    InsertAllowed = false;
    ModifyAllowed = false;
    //para ordenar mejor crear una key en la tabla con el campo systemat y utulizar sourcetableview en lugar del trigger openpage de la pagina
    SourceTableView = sorting(SystemCreatedAt) order(descending); // ordena la pag por este campo de manera descendente

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Id; Rec.Id)
                {
                    Visible = false;
                    Caption = 'Id';
                    ToolTip = 'Especifica el Id';
                    ApplicationArea = All;
                }
                field(Mensaje; Rec.Mensaje)
                {
                    Caption = 'Mensaje';
                    ToolTip = 'Especifica el mensaje';
                    ApplicationArea = All;
                }
                field(SystemCreatedAt; Rec.SystemCreatedAt)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedAt field.';
                    ApplicationArea = All;
                }
            }
        }
    }
    //mejor hacer esto como hemos puesto arriba con sourcetableview
    // trigger OnOpenPage()
    // begin
    //     rec.SetCurrentKey(SystemCreatedAt);
    //     rec.SetAscending(SystemCreatedAt, false);
    // end;

}