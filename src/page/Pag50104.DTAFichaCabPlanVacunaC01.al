/// <summary>
/// Ficha cabeceras del plan de vacunacion
/// </summary>
page 50104 "DTAFichaCabPlanVacunaC01"
{
    Caption = 'Ficha Cab. Plan Vacunación';
    UsageCategory = Administration;
    ApplicationArea = all;
    PageType = Document;
    SourceTable = DTACabeceraPlanVacunacionC01;

    layout
    {
        area(content)
        {
            group(General)
            {
                field(Cod; Rec.CodigoCabecera)
                {
                    Caption = 'código';
                    ToolTip = 'Specifies the value of the Código field.';
                    ApplicationArea = All;
                }
                field(Descripcion; Rec.Descripcion)
                {
                    Caption = 'Descripción';
                    ToolTip = 'Specifies the value of the Descripción field.';
                    ApplicationArea = All;
                }
                field(FechaInicioVacunacion; Rec.FechaInicioVacunacion)
                {
                    ToolTip = 'Specifies the value of the Fecha Inicio Vacunación field.';
                    ApplicationArea = All;
                    Caption = 'Fecha inicio vacunación';
                }
                field(EmpresaVacunadora; Rec.EmpresaVacunadora)
                {
                    ToolTip = 'Specifies the value of the Empresa Vacunadora field.';
                    ApplicationArea = All;
                    Caption = 'Código Empresa vacunadora';
                }
                field(NombreEmpresaVacunadora; rec.NombreEmpresaVacunadoraF())
                {
                    Caption = 'Nombre Empresa vacunadora';
                    ApplicationArea = All;
                    ToolTip = 'Aparecerá el nombre del proveedor que realizará la vacunación';
                }
            }
            part(Lineas; DTASubPageLineasVacunacionC01)
            {
                ApplicationArea = all;
                SubPageLink = CodigoLineas = field(CodigoCabecera);
            }
        }

    }
    actions
    {
        area(Processing)
        {
            action(EjemploVariables01)
            {
                ApplicationArea = All;
                Caption = 'Ejemplo de variables 01';

                trigger OnAction()
                var
                    culDTAFuncionesAppC01: Codeunit DTAFuncionesAppC01;

                    xlTexto: Text;
                begin
                    xlTexto := 'david';
                    culDTAFuncionesAppC01.EjemploVariablesESF(xlTexto);
                    Message(xlTexto);
                end;
            }
            action(EjemploVariables02)
            {
                ApplicationArea = All;
                Caption = 'Ejemplo de variables Objetos';

                trigger OnAction()
                var
                    culDTAFuncionesAppC01: Codeunit DTAFuncionesAppC01;
                    xlTexto: TextBuilder;
                begin
                    xlTexto.Append('david');
                    culDTAFuncionesAppC01.EjemploVariablesES02F(xlTexto);
                    Message(xlTexto.ToText());
                end;
            }
            action(bingo)
            {
                ApplicationArea = all;
                Caption = 'jugar a bingo';
                trigger OnAction()
                var
                    culDTAFuncionesAppC01: Codeunit DTAFuncionesAppC01;
                begin
                    culDTAFuncionesAppC01.BingoF();
                end;
            }
            action(PruebaFunSegPlano)
            {
                ApplicationArea = all;
                Caption = 'prueba funcion en segundo plano';
                trigger OnAction()
                var
                    xlSesionIniciada: Integer;
                begin
                    // codeunit.Run(codeunit::DTACodeunitLentaC01);
                    StartSession(xlSesionIniciada, codeunit::DTACodeunitLentaC01);
                    Message('proceso lanzado en segundo plano');
                end;
            }
            action(PruebaFunPrimerPlano)
            {
                ApplicationArea = all;
                Caption = 'prueba funcion en primer plano';
                trigger OnAction()
                begin
                    codeunit.run(Codeunit::DtacodeunitLentaC01);
                end;
            }
            action(Logs)
            {
                ApplicationArea = All;
                Caption = 'logs';
                Image = Log;
                RunObject = page DTALogsC01;

            }
            action(CrearLineas)
            {
                ApplicationArea = All;
                Caption = 'Crear Lineas';
                Image = Create;
                trigger OnAction()
                var
                    rlDTALineasPlanVacunacion: record DTALineasPlanVacunacionC01;
                    xlInicio: DateTime;
                    i: Integer;
                begin
                    xlInicio := CurrentDateTime; // fecha y hora actual
                    for i := 1 to 1000 do begin

                        rlDTALineasPlanVacunacion.Init();
                        rlDTALineasPlanVacunacion.Validate(rlDTALineasPlanVacunacion.CodigoLineas, rec.CodigoCabecera);
                        rlDTALineasPlanVacunacion.Validate(rlDTALineasPlanVacunacion.NumLinea, i);
                        rlDTALineasPlanVacunacion.Insert(true);
                    end;
                    Message('esto tarda %1', CurrentDateTime - xlInicio);
                end;
            }
            action(CrearLineasNoBulk)
            {
                ApplicationArea = All;
                Caption = 'Crear Lineas bulk';
                Image = Create;
                trigger OnAction()
                var
                    rlDTALineasPlanVacunacion: record DTALineasPlanVacunacionC01;
                    xlInicio: DateTime;
                    i: Integer;
                begin
                    xlInicio := CurrentDateTime; // fecha y hora actual
                    for i := 1 to 1000 do begin

                        rlDTALineasPlanVacunacion.Init(); // importante inicializar cada registro dentro del bucle for y limpiar cada vez el registro para que cree uno nuevo
                        rlDTALineasPlanVacunacion.Validate(rlDTALineasPlanVacunacion.CodigoLineas, rec.CodigoCabecera);
                        rlDTALineasPlanVacunacion.Validate(rlDTALineasPlanVacunacion.NumLinea, i);
                        if not rlDTALineasPlanVacunacion.Insert(true) then begin
                            Error('no se ha podido insetar la linea %1 del plan %2', rlDTALineasPlanVacunacion.NumLinea, rlDTALineasPlanVacunacion.CodigoLineas);
                        end;
                    end;
                    Message('esto tarda %1', CurrentDateTime - xlInicio);
                end;




            }

        }
    }
}
