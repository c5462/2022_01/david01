page 50109 "DTAEjDicC01"
{
    ApplicationArea = All;
    Caption = 'Ejemplo Diccionarios';
    PageType = List;
    SourceTable = Integer;
    UsageCategory = Lists;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Num; rec.Number)
                {
                    ApplicationArea = All;
                    Caption = 'Nº';
                }
                field(Campo01; ValorCeldaF(rec.Number, 1))
                {
                    ApplicationArea = All;
                    Caption = 'valor celda 01';
                }
                field(Campo02; ValorCeldaF(rec.Number, 2))
                {
                    ApplicationArea = All;
                    Caption = 'valor celda 02';
                }
                field(Campo03; ValorCeldaF(rec.Number, 3))
                {
                    ApplicationArea = All;
                    Caption = 'valor celda 03';
                }
                field(Campo04; ValorCeldaF(rec.Number, 4))
                {
                    ApplicationArea = All;
                    Caption = 'valor celda 04';
                }
                field(Campo05; ValorCeldaF(rec.Number, 5))
                {
                    ApplicationArea = All;
                    Caption = 'valor celda 05';
                }
                field(Campo06; ValorCeldaF(rec.Number, 6))
                {
                    ApplicationArea = All;
                    Caption = 'valor celda 06';
                }
                field(Campo07; ValorCeldaF(rec.Number, 7))
                {
                    ApplicationArea = All;
                    Caption = 'valor celda 07';
                }
                field(Campo08; ValorCeldaF(rec.Number, 8))
                {
                    ApplicationArea = All;
                    Caption = 'valor celda 08';
                }
                field(Campo09; ValorCeldaF(rec.Number, 9))
                {
                    ApplicationArea = All;
                    Caption = 'valor celda 09';
                }
                field(Campo10; ValorCeldaF(rec.Number, 10))
                {
                    ApplicationArea = All;
                    Caption = 'valor celda 10';
                }


            }
        }
    }
    var

        xDicc: Dictionary of [Integer, List of [decimal]];

    local procedure ValorCeldaF(pFila: Integer; pColumna: Integer): Decimal
    begin
        exit(xDicc.Get(pFila).Get(pColumna));
    end;

    trigger OnOpenPage()
    var
        xlFila: Integer;
        xlColumna: Integer;
        xlNumeros: List of [Decimal];
    begin
        for xlFila := 1 to 10 do begin
            Clear(xlNumeros);
            for xlColumna := 1 to 10 do begin
                xlNumeros.Add(random(55555));

            end;
            xDicc.Add(xlfila, xlNumeros);
        end;
        rec.SetRange(Number, 1, 10);
        EjemploNotificacionF();
    end;

    local procedure EjemploNotificacionF()
    var
        xlNotificacion: Notification;
    begin
        xlNotificacion.Id(CreateGuid()); // si mandamos varias notificaciones hay que ponerle id
        xlNotificacion.Message('El cliente no tiene ninguna vacuna');
        xlNotificacion.AddAction('Abrir lista de clientes', codeunit::DTAFuncionesAppC01, 'AbrirListaClientesF');
        xlNotificacion.SetData('CodigoCliente', '10000');
        xlNotificacion.Send();
    end;

}
