/// <summary>
/// Pagina de entrada de datos
/// </summary>
page 50108 "DTAEntradaDeDatosVariosC01"
{
    Caption = 'Entrada De Datos Varios';
    PageType = StandardDialog;


    layout
    {
        area(content)
        {
            field(Texto01; mTextos[1])
            {
                ApplicationArea = All;
                visible = xTxtVisible01;
                CaptionClass = mCaptionClass[xTipoDato::Texto, 1];
            }
            field(Texto02; mTextos[2])
            {
                ApplicationArea = All;
                visible = xTxtVisible02;
                CaptionClass = mCaptionClass[xTipoDato::Texto, 2];
            }
            field(Texto03; mTextos[3])
            {
                ApplicationArea = All;
                visible = xTxtVisible03;
                CaptionClass = mCaptionClass[xTipoDato::Texto, 3];
            }
            field(Texto04; mTextos[4])
            {
                ApplicationArea = All;
                visible = xTxtVisible04;
                CaptionClass = mCaptionClass[xTipoDato::Texto, 4];
            }
            field(Booleano1; mbooleanos[1])
            {
                ApplicationArea = All;
                visible = xBooleanoVisible01;
                CaptionClass = mCaptionClass[xtipodato::booleano, 1];
            }
            field(Booleano2; mbooleanos[2])
            {
                ApplicationArea = All;
                visible = xBooleanoVisible02;
                CaptionClass = mCaptionClass[xtipodato::booleano, 2];
            }
            field(Booleano3; mbooleanos[3])
            {
                ApplicationArea = All;
                visible = xBooleanoVisible03;
                CaptionClass = mCaptionClass[xtipodato::booleano, 3];
            }
            field(numero01; mNumeros[1])
            {
                ApplicationArea = All;
                visible = xNumeroVisible01;
                CaptionClass = mCaptionClass[xtipodato::Numerico, 1];
            }
            field(numero02; mNumeros[2])
            {
                ApplicationArea = All;
                visible = xNumeroVisible02;
                CaptionClass = mCaptionClass[xtipodato::Numerico, 2];
            }
            field(numero03; mNumeros[3])
            {
                ApplicationArea = All;
                visible = xNumeroVisible03;
                CaptionClass = mCaptionClass[xtipodato::Numerico, 3];
            }
            field(Fecha01; mFechas[1])
            {
                ApplicationArea = All;
                visible = xFechaVisible01;
                CaptionClass = mCaptionClass[xtipodato::Fecha, 1];
            }
            field(Fecha02; mFechas[2])
            {
                ApplicationArea = All;
                visible = xFechaVisible02;
                CaptionClass = mCaptionClass[xtipodato::Fecha, 2];
            }
            field(Fecha03; mFechas[3])
            {
                ApplicationArea = All;
                visible = xFechaVisible03;
                CaptionClass = mCaptionClass[xtipodato::Fecha, 3];
            }
            field(Password; mpassword[1])
            {
                ApplicationArea = All;
                visible = xPasswordVisible01;
                CaptionClass = mCaptionClass[xtipodato::password, 1];
                ExtendedDatatype = Masked;
            }
            field(NuevoPassword; mpassword[2])
            {
                ApplicationArea = All;
                visible = xPasswordVisible02;
                CaptionClass = mCaptionClass[xtipodato::password, 2];
                ExtendedDatatype = Masked;
            }
            field(RepitePassword; mpassword[3])
            {
                ApplicationArea = All;
                visible = xPasswordVisible03;
                CaptionClass = mCaptionClass[xtipodato::password, 3];
                ExtendedDatatype = Masked;
            }


        }
    }
    var
        mTextos: array[10] of Text;
        mbooleanos: array[3] of Boolean;

        mPunteros: array[8, 10] of Integer;
        xTipoPuntero: Option Nulo,Pasado,Leido;
        mCaptionClass: Array[8, 10] of Text;
        xTipoDato: Option Nulo,Texto,Booleano,Numerico,Fecha,password;
        xTxtVisible01: Boolean;
        xTxtVisible02: Boolean;
        xTxtVisible03: Boolean;
        xBooleanoVisible01: Boolean;
        xBooleanoVisible02: Boolean;
        xBooleanoVisible03: Boolean;

        mNumeros: Array[10] of Decimal;
        xNumeroVisible01: Boolean;
        xNumeroVisible02: Boolean;
        xNumeroVisible03: Boolean;
        mFechas: Array[10] of Date;
        xFechaVisible01: Boolean;
        xFechaVisible02: Boolean;
        xFechaVisible03: Boolean;
        xTxtVisible04: Boolean;
        mpassword: Array[10] of Text;
        xPasswordVisible01: Boolean;
        xPasswordVisible02: Boolean;
        xPasswordVisible03: Boolean;

    /// <summary>
    /// Hace que se muestre un campo tipo texto con el caption y valor pasados
    /// </summary>
    /// <param name="pCaption">Text.</param>
    /// <param name="pValorInicial">Text.</param>
    procedure CampoF(pCaption: Text; pValorInicial: Text)

    begin
        mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto] += 1;
        mCaptionClass[xtipodato::texto, mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto]] := pCaption;
        mTextos[mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto]] := pValorInicial;
        xtxtvisible01 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto] >= 1;
        xtxtvisible02 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto] >= 2;
        xtxtvisible03 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto] >= 3;
        xTxtVisible04 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto] >= 4;
    end;

    /// <summary>
    /// Hace que se muestre un campo tipo booleano con el caption y valor pasados
    /// </summary>
    /// <param name="pCaption">Text.</param>
    /// <param name="pValorInicial">Text.</param>
    procedure CampoF(pCaption: Text; pValorInicial: Boolean)

    begin
        mPunteros[xTipoPuntero::Pasado, xTipoDato::Booleano] += 1;
        mCaptionClass[xTipoDato::booleano, mPunteros[xTipoPuntero::Pasado, xTipoDato::Booleano]] := pCaption;
        mbooleanos[mPunteros[xTipoPuntero::Pasado, xTipoDato::Booleano]] := pValorInicial;
        xBooleanoVisible01 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Booleano] >= 1;
        xBooleanoVisible02 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Booleano] >= 2;
        xBooleanoVisible03 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Booleano] >= 3;

    end;

    /// <summary>
    /// Hace que se muestre un campo tipo numerico decimal con el caption y valor pasados
    /// </summary>
    /// <param name="pCaption">Text.</param>
    /// <param name="pValorInicial">Text.</param>
    procedure CampoF(pCaption: Text; pValorInicial: Decimal)

    begin
        mPunteros[xTipoPuntero::Pasado, xTipoDato::Numerico] += 1;
        mCaptionClass[xtipodato::Numerico, mPunteros[xTipoPuntero::Pasado, xTipoDato::Numerico]] := pCaption;
        mNumeros[mPunteros[xTipoPuntero::Pasado, xTipoDato::Numerico]] := pValorInicial;
        xnumerovisible01 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Numerico] >= 1;
        xnumerovisible02 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Numerico] >= 2;
        xnumerovisible03 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Numerico] >= 3;
    end;

    /// <summary>
    /// Hace que se muestre un campo tipo fecha con el caption y valor pasados
    /// </summary>
    /// <param name="pCaption">Text.</param>
    /// <param name="pValorInicial">Text.</param>
    procedure CampoF(pCaption: Text; pValorInicial: Date)

    begin
        mPunteros[xTipoPuntero::Pasado, xTipoDato::Fecha] += 1;
        mCaptionClass[xtipodato::Fecha, mPunteros[xTipoPuntero::Pasado, xTipoDato::Fecha]] := pCaption;
        mFechas[mPunteros[xTipoPuntero::Pasado, xTipoDato::Fecha]] := pValorInicial;
        xfechavisible01 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Fecha] >= 1;
        xfechavisible02 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Fecha] >= 2;
        xfechavisible03 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Fecha] >= 3;
    end;

    /// <summary>
    /// Hace que se muestre un campo tipo texto password y nuevo passwiord con el caption y valor pasados
    /// </summary>
    /// <param name="pCaption">Text.</param>
    /// <param name="pValorInicial">Text.</param>
    /// <param name="pPassword">Boolean.</param>
    /// <param name="pValorNuevo">Text.</param>
    procedure CampoF(pCaption: Text; pValorInicial: Text; pPassword: Boolean)

    begin
        if pPassword then begin


            mPunteros[xTipoPuntero::Pasado, xTipoDato::password] += 1;
            mCaptionClass[xtipodato::password, mPunteros[xTipoPuntero::Pasado, xTipoDato::password]] := pCaption;
            mpassword[mPunteros[xTipoPuntero::Pasado, xTipoDato::password]] := pValorInicial;
            xpasswordvisible01 := mPunteros[xTipoPuntero::Pasado, xTipoDato::password] >= 1;
            xpasswordvisible02 := mPunteros[xTipoPuntero::Pasado, xTipoDato::password] >= 2;
            xpasswordvisible03 := mPunteros[xTipoPuntero::Pasado, xTipoDato::password] >= 3;
        end else begin
            CampoF(pCaption, pValorInicial);
        end;
    end;

    /// <summary>
    /// Devuelve el texto introducido por el usuario
    /// </summary>
    /// <returns>Return value of type Text.</returns>
    procedure CampoF(): Text

    begin
        mPunteros[xTipoPuntero::Leido, xTipoDato::Texto] += 1;
        exit(mtextos[mPunteros[xTipoPuntero::Leido, xTipoDato::Texto]]);
    end;
    /// <summary>
    /// Devuelve el booleano introducido por el usuario
    /// </summary>
    /// <param name="pSalida">VAR Boolean.</param>
    /// <returns>Return value of type Text.</returns>
    procedure CampoF(var pSalida: Boolean): Boolean

    begin
        mPunteros[xTipoPuntero::Leido, xTipoDato::Booleano] += 1;
        psalida := mbooleanos[mPunteros[xTipoPuntero::Leido, xTipoDato::Booleano]];
        exit(pSalida);
    end;
    /// <summary>
    /// Devuelve el booleano introducido por el usuario
    /// </summary>
    /// <param name="pSalida">VAR Boolean.</param>
    /// <returns>Return value of type Text.</returns>
    procedure CampoF(var pSalida: Decimal): Decimal

    begin
        mPunteros[xTipoPuntero::Leido, xTipoDato::Numerico] += 1;
        psalida := mNumeros[mPunteros[xTipoPuntero::Leido, xTipoDato::Numerico]];
        exit(pSalida);
    end;
    /// <summary>
    /// Devuelve el booleano introducido por el usuario
    /// </summary>
    /// <param name="pSalida">VAR Boolean.</param>
    /// <returns>Return value of type Text.</returns>
    procedure CampoF(var pSalida: Date): Date

    begin
        mPunteros[xTipoPuntero::Leido, xTipoDato::Fecha] += 1;
        psalida := mFechas[mPunteros[xTipoPuntero::Leido, xTipoDato::Fecha]];
        exit(pSalida);
    end;

    procedure CampoF(var psalida: Text): Text

    begin
        psalida := mpassword[mPunteros[xTipoPuntero::Leido, xTipoDato::password]];
        exit(psalida);
    end;

    // a partir de aqui funciones para elegir que campos queremos devolver en funcion del puntero

    procedure CampoF(pPuntero: integer): Text

    begin

        exit(mtextos[pPuntero]);
    end;

    procedure CampoF(var psalida: Text; pPuntero: integer): Text

    begin
        psalida := mpassword[pPuntero];
        exit(psalida);
    end;

    procedure CampoF(var pSalida: Boolean; pPuntero: integer): Boolean

    begin

        psalida := mbooleanos[pPuntero];
        exit(pSalida);
    end;

    procedure CampoF(var pSalida: Decimal; pPuntero: integer): Decimal

    begin
        psalida := mNumeros[pPuntero];
        exit(pSalida);
    end;

    procedure CampoF(var pSalida: Date; pPuntero: integer): Date

    begin
        psalida := mFechas[pPuntero];
        exit(pSalida);
    end;
}
