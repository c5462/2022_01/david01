/// <summary>
/// Ficha varios del plan de vacunacion
/// </summary>
page 50102 "DTAFichaDeVariosC01"
{
    Caption = 'Ficha De registro Varios';
    UsageCategory = Administration;
    ApplicationArea = all;
    PageType = Card;
    SourceTable = DTAVariosC01;
    AdditionalSearchTerms = 'curso01';
    AboutTitle = 'Ficha de tabla varios';
    AboutText = 'Se usará para editar registros de una tabla donde tenemos tipos varios de registros';

    layout
    {
        area(content)
        {
            group(General)
            {
                field(Codigo; Rec.Codigo)
                {
                    ToolTip = 'Specifies the value of the Código field.';
                    ApplicationArea = All;
                }
                field(Tipo; Rec.Tipo)
                {
                    ToolTip = 'Specifies the value of the Tipo field.';
                    ApplicationArea = All;
                    AboutTitle = 'tipo de registro';
                    AboutText = '';
                }
                field(Descripcion; Rec.Descripcion)
                {
                    ToolTip = 'Specifies the value of the Descripción field.';
                    ApplicationArea = All;
                }
                field(Bloqueado; Rec.Bloqueado)
                {
                    ToolTip = 'Specifies the value of the MyField field.';
                    ApplicationArea = All;
                }
                field(FechaSegundaDosis; rec.FechaSegundaDosis)
                {
                    ToolTip = 'especifica la fecha de la segunda dosis';
                    ApplicationArea = all;
                }
            }
        }
    }
}
