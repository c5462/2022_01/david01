page 50100 "DTAConfiguracionC01"
{
    Caption = 'Configuración de la app 01 del curso';
    PageType = Card;
    SourceTable = DTAConfiguracionC01;
    AboutTitle = 'En esta página crearemos la configuración de la primera app del curso de Especialistas de Bussiness Central 2022';
    AboutText = 'Configure la app de forma correcta. Asegurese que asigna el valor del campo de cliente web';
    AdditionalSearchTerms = 'Mi priemra APP';
    UsageCategory = Administration;
    ApplicationArea = All;
    DeleteAllowed = false;
    InsertAllowed = false;
    ShowFilter = true;

    layout
    {
        area(content)
        {
            group(General)
            {

                field(CodCteWeb; Rec.CodCteWeb)
                {
                    ToolTip = 'Configure la app de forma correcta. Asegurese que asigna el valor del campo de cliente web';
                    ApplicationArea = All;
                    AboutTitle = 'Campo Code. cliente web';
                    AboutText = 'Especifique el cliente de BC que se utilizará para los pedidos que entren por web';
                    Importance = Promoted;
                }
                field(TextoRegistro; Rec.TextoRegistro)
                {
                    ToolTip = 'Specifies the value of the Texto Registro field.';
                    ApplicationArea = All;
                    AboutTitle = 'texto registro git';
                    AboutText = 'texto utilizado de prueba para control de versiones git';
                }
                field(TipoDoc; Rec.TipoDoc)
                {
                    ToolTip = 'Specifies the value of the Tipo de documento field.';
                    ApplicationArea = All;
                }
                field(ColorFondo; Rec.ColorFondo)
                {
                    ToolTip = 'Specifies the value of the Tipo de documento field.';
                    ApplicationArea = All;
                }
                field(ColorLetra; Rec.ColorLetra)
                {
                    ToolTip = 'Specifies the value of the Tipo de documento field.';
                    ApplicationArea = All;
                }
                field(codTabla; Rec.codTabla)
                {
                    ToolTip = 'Specifies the value of the Tipo de documento field.';
                    ApplicationArea = All;
                }
                field(NombreTabla; rec.NombreTablaF(rec.TipoTabla, rec.codTabla))
                {
                    ApplicationArea = All;
                    Caption = 'Nombre Tabla';
                    Editable = false;
                }

                field(TipoTabla; Rec.TipoTabla)
                {
                    ToolTip = 'Specifies the value of the Tipo de documento field.';
                    ApplicationArea = All;
                }

            }
            group(InfInterna)
            {
                field(SystemCreatedAt; Rec.SystemCreatedAt)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedAt field.';
                    ApplicationArea = All;
                }
                field(SystemCreatedBy; Rec.SystemCreatedBy)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedBy field.';
                    ApplicationArea = All;
                }
                field(SystemId; Rec.SystemId)
                {
                    ToolTip = 'Specifies the value of the SystemId field.';
                    ApplicationArea = All;
                    Importance = Additional;
                }
                field(SystemModifiedAt; Rec.SystemModifiedAt)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedAt field.';
                    ApplicationArea = All;
                }
                field(SystemModifiedBy; Rec.SystemModifiedBy)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedBy field.';
                    ApplicationArea = All;
                }
            }
            group(CamposCalculados)
            {
                Caption = 'campos calculado';
                field(NombreCliente; rec.NombreCliente)
                {
                    ToolTip = 'Campo calculado que nos muestra nombre del cliente';
                    ApplicationArea = All;

                }
                field(NombreClientePorfuncion; rec.NombreClienteF(rec.CodCteWeb))
                {
                    ApplicationArea = All;
                    Caption = 'Nombre cliente';
                }
                field(NumUnidadesVendidas; Rec.UnidadesDisponibles)
                {
                    ToolTip = 'Total de unidades vendidas';
                    ApplicationArea = all;
                }
                field(Password; rec.IdPassword)
                {

                    ApplicationArea = all;
                    Caption = 'password';
                }
                field(NumIteraciones; xNumIteraciones)
                {

                    ApplicationArea = all;
                    Caption = 'Numero de iteraciones';
                }


            }
        }
    }
    actions
    {
        area(Processing)
        {

            action(ejemplo01)
            {
                ApplicationArea = All;
                Caption = 'mensaje de acción';
                Image = AboutNav;

                trigger OnAction()
                begin
                    Message('Accion pulsada');
                end;
            }
            action(PedirDatos)
            {
                ApplicationArea = All;
                Caption = 'Pedir datos varios';

                trigger OnAction()
                var
                    pglDTAEntradaDeDatosC01: Page DTAEntradaDeDatosVariosC01;
                    xlBool: Boolean;
                    xlNumerico: Decimal;
                    xlFecha: Date;

                begin
                    pglDTAEntradaDeDatosC01.CampoF('Ciudad de nacimiento', 'Albacete');
                    pglDTAEntradaDeDatosC01.CampoF('Pais de nacimiento', 'España');
                    pglDTAEntradaDeDatosC01.CampoF('Comunidad de nacimiento', 'CLM');
                    pglDTAEntradaDeDatosC01.CampoF('Cuota primer trimestre pagada', false);
                    pglDTAEntradaDeDatosC01.CampoF('Cuota segundo trimestre pagada', true);
                    pglDTAEntradaDeDatosC01.CampoF('importe 1 decimal introducido', 2.13);
                    pglDTAEntradaDeDatosC01.CampoF('importe 2 decimal introducido', 3.13);
                    pglDTAEntradaDeDatosC01.CampoF('importe 3 decimal introducido', 4.13);
                    pglDTAEntradaDeDatosC01.CampoF('fecha 1 introducida', 0D);
                    pglDTAEntradaDeDatosC01.CampoF('fecha 2 introducida', 0D);
                    pglDTAEntradaDeDatosC01.CampoF('fecha 3 introducida', 0D);
                    if pglDTAEntradaDeDatosC01.RunModal() in [Action::LookupOK, action::OK, action::Yes] then begin
                        Message(pglDTAEntradaDeDatosC01.CampoF());
                        Message(pglDTAEntradaDeDatosC01.CampoF());
                        Message(pglDTAEntradaDeDatosC01.CampoF());
                        Message('cuota 1 %1', pglDTAEntradaDeDatosC01.CampoF(xlBool));
                        pglDTAEntradaDeDatosC01.CampoF(xlBool);
                        Message('cuota 2 %1,', xlBool);

                        pglDTAEntradaDeDatosC01.CampoF(xlNumerico);
                        Message('importe decimal 1 %1,', xlNumerico);
                        pglDTAEntradaDeDatosC01.CampoF(xlNumerico);
                        Message('importe decimal 2 %1,', xlNumerico);
                        pglDTAEntradaDeDatosC01.CampoF(xlNumerico);
                        Message('importe decimal 3 %1,', xlNumerico);

                        pglDTAEntradaDeDatosC01.CampoF(xlFecha);
                        Message('fecha 1 %1,', xlFecha);
                        pglDTAEntradaDeDatosC01.CampoF(xlFecha);
                        Message('fecha 2 %1,', xlFecha);
                        pglDTAEntradaDeDatosC01.CampoF(xlFecha);
                        Message('fecha 3 %1,', xlFecha);
                    end else begin
                        Error('Proceso cancelado por el usuario');
                    end;
                end;
            }

            action(CambiarPassword)
            {
                ApplicationArea = All;
                Caption = 'cambiar password';
                trigger OnAction()
                begin
                    CambiarPasswordF();
                end;
            }
            action(ExportarVacunas)
            {
                Caption = 'Exportar Vacunas';
                ApplicationArea = All;

                trigger OnAction()
                var

                begin
                    ExportarVacunasF();
                end;
            }
            action(ClientesAlmacenGris)
            {
                ApplicationArea = All;
                Caption = 'Clientes Almacen Gris';

                trigger OnAction()
                var
                    rlCustomer: record customer;
                    xlFiltroGrupo: Integer;
                    rlCompany: Record Company;
                begin
                    xlFiltroGrupo := rlCustomer.Count;
                    //MOSTRAR LA LISTA DE EMPRESAS DE LA BASE DE DATOS Y DE LA SELECCIONADA MOSTRAR SUS CLIENTES GRISES
                    if AccionAceptadaF(page.RunModal(page::Companies, rlCompany)) then begin
                        rlCustomer.ChangeCompany(rlCompany.Name);

                        xlFiltroGrupo := rlCustomer.FilterGroup();
                        rlCustomer.FilterGroup(xlFiltroGrupo + 20); // creamos filtro para que usuario no pueda elegir y quitar filtros
                        rlCustomer.SetRange("Location Code", 'GRIS');
                        rlCustomer.FilterGroup(xlFiltroGrupo); // devolvemos filtro original
                        if AccionAceptadaF(page.RunModal(page::"Customer List", rlCustomer)) then begin
                            Message('proceso OK');
                        end else begin
                            Error('proceso cancelado por el usuario');
                        end;
                    end else begin
                        Error('Proceso cancelado por el usuario');
                    end;

                end;
            }
            action(ImportarVacunas)
            {
                ApplicationArea = All;
                Caption = 'Importar Vacunas';

                trigger OnAction()
                begin
                    ImportarVacunasF();
                end;
            }
            action(CuentaClientesEnMovClientes)
            {
                ApplicationArea = All;
                Caption = 'Cuenta clientes en Mov Clientes';

                trigger OnAction()
                var
                    rlCustLedgerEntry: Record "Cust. Ledger Entry";
                    xlPaginaFiltros: FilterPageBuilder;
                    xlNumclientes: Integer;
                    clMovimientos: Label 'Movimientos';
                    xlListaClientes: List of [code[20]];
                // xlLista2: List of [Decimal];
                // xlDic: Dictionary of [Code[20], Decimal];
                // xlDic2: Dictionary of [Code[20], List of [Decimal]];
                begin
                    //xlDic2.Add()
                    xlPaginaFiltros.PageCaption('Seleccione los clientes a procesar');
                    xlPaginaFiltros.AddRecord(clMovimientos, rlCustLedgerEntry);
                    xlPaginaFiltros.AddField(clMovimientos, rlCustLedgerEntry."Posting Date");
                    if xlPaginaFiltros.RunModal() then begin
                        rlCustLedgerEntry.SetView(xlPaginaFiltros.GetView(clMovimientos, true));
                        rlCustLedgerEntry.SetLoadFields("Customer No.");
                        if rlCustLedgerEntry.FindSet(false) then begin
                            repeat
                                //contar los clientes a traves de una lista
                                //añadir codigo cliente a lista
                                if not xlListaClientes.Contains(rlCustLedgerEntry."Customer No.") then begin
                                    xlListaClientes.Add(rlCustLedgerEntry."Customer No.");
                                end;
                            until rlCustLedgerEntry.Next() = 0;
                            // contar clientes de la lista y mostrar mensaje
                            xlNumclientes := xlListaClientes.Count;
                            Message('El numero de clientes es: %1', xlNumclientes);
                        end else begin
                            Message('No existen registros dentro del filtro %1', rlCustLedgerEntry.GetFilters);
                        end;
                    end else begin
                        Message('Proceso cancelado por el usuario');
                    end;
                end;
            }
            action(DTAPruebaTxtC01)
            {
                ApplicationArea = All;
                Caption = 'Pruebas con Text';

                trigger OnAction()
                var
                    xlInicio: DateTime;
                    i: Integer;
                    xlTxt: Text;
                begin
                    xlInicio := CurrentDateTime;
                    for i := 1 to xNumIteraciones do begin
                        xlTxt += '.';
                    end;
                    Message('tiempo invertido %1', CurrentDateTime - xlInicio);
                end;
            }
            action(DTAPruebaTextBuilderC01)
            {
                ApplicationArea = All;
                Caption = 'Pruebas con Textbuilder';

                trigger OnAction()
                var
                    i: Integer;
                    xlInicio: DateTime;
                    xlTxt: TextBuilder;
                begin
                    xlInicio := CurrentDateTime;
                    for i := 1 to xNumiteraciones do begin
                        xlTxt.Append('.');
                    end;
                    Message('tiempo invertido %1', CurrentDateTime - xlInicio);

                end;
            }

        }
    }
    trigger OnOpenPage()
    begin
        rec.GetF();
    end;

    [NonDebuggable]
    local procedure CambiarPasswordF()
    var
        pglDTAEntraDeDatosVariosC01: Page DTAEntradaDeDatosVariosC01;
        xlPass: Text;
    begin
        pglDTAEntraDeDatosVariosC01.CampoF('password actual', '', true);
        pglDTAEntraDeDatosVariosC01.CampoF('nuevo password', '', true);
        pglDTAEntraDeDatosVariosC01.CampoF('repita nuevo password', '', true);
        if pglDTAEntraDeDatosVariosC01.RunModal() in [Action::LookupOK, action::OK, action::Yes] then begin
            // compruebo si el password que escribe usuario coincide con el guardado en el campo idpassword de la tabla
            // me traigo el password guardado en tabla a traves de la funcion para devolver password guardado
            if pglDTAEntraDeDatosVariosC01.CampoF(xlPass) = rec.passwordF() then begin
                //compruebo si el password 2 es el igual password 3 introducido por usuario. xlpass es un punto en campoF que se incrementa con cada llamada a la funcion
                if pglDTAEntraDeDatosVariosC01.CampoF(xlPass) = pglDTAEntraDeDatosVariosC01.CampoF(xlPass) then begin
                    //inserto el nuevo password en la tabla a traves de la funcion de asignar password
                    rec.passwordF(xlPass);
                    Message('Password cambiado');
                end else begin
                    Error('Nuevo password no coincide');
                end;
            end else begin
                Error('Password actual no coincide');
            end;
        end else begin
            Error('Proceso cancelado por el usuario');
        end;
    end;

    local procedure ExportarVacunasF()
    var
        rlDTACabeceraPlanVacunacionC01: Record DTACabeceraPlanVacunacionC01;
        pglDTAListaCabPlanVacunacionC01: Page DTAListaCabPlanVacunacionC01;
        TempLDTAConfiguracionC01: Record DTAConfiguracionC01 temporary;
        rlDTAlineasPlanVacunacionC01: record DTALineasPlanVacunacionC01;
        clSeparador: Label '·';
        xlInStr: InStream;
        xlOutStr: OutStream;
        xlLinea: Text;
        xlNombreFichero: Text;
    begin
        Message('Selecciones vacunas a exportar');
        pglDTAListaCabPlanVacunacionC01.LookupMode(true);
        pglDTAListaCabPlanVacunacionC01.SetRecord(rlDTACabeceraPlanVacunacionC01);
        if pglDTAListaCabPlanVacunacionC01.RunModal() in [Action::LookupOK, action::OK, action::yes] then begin
            // obtenemos el dataset con los registros seleccionado por el usuario hay que crear pagina como variable
            pglDTAListaCabPlanVacunacionC01.GetSelectionFilterF(rlDTACabeceraPlanVacunacionC01); // traemos a modo de filtro lo seleccionado por el cliente en la pagina

            if rlDTACabeceraPlanVacunacionC01.FindSet(false) then begin

                TempLDTAConfiguracionC01.MiBlob.CreateOutStream(xloutStr, TextEncoding::Windows);
                repeat
                    //recorremos el registro de cabeceras
                    xlOutStr.WriteText('C');
                    xlOutStr.WriteText(clSeparador);
                    xlOutStr.WriteText(rlDTACabeceraPlanVacunacionC01.NombreEmpresaVacunadoraF());
                    xlOutStr.WriteText(clSeparador);
                    xlOutStr.WriteText(rlDTACabeceraPlanVacunacionC01.CodigoCabecera);
                    xlOutStr.WriteText(clSeparador);
                    xlOutStr.WriteText(rlDTACabeceraPlanVacunacionC01.Descripcion);
                    xlOutStr.WriteText(clSeparador);
                    xlOutStr.WriteText(rlDTACabeceraPlanVacunacionC01.EmpresaVacunadora);
                    xlOutStr.WriteText(clSeparador);
                    xlOutStr.WriteText(format(rlDTACabeceraPlanVacunacionC01.FechaInicioVacunacion));
                    xlOutStr.WriteText();
                    // recorremos las lineas
                    rlDTAlineasPlanVacunacionC01.SetRange(CodigoLineas, rlDTACabeceraPlanVacunacionC01.CodigoCabecera); // primero tenemos que poner el filtro
                    if rlDTAlineasPlanVacunacionC01.FindSet(false) then begin
                        repeat
                            xlOutStr.WriteText('L');
                            xlOutStr.WriteText(clSeparador);
                            xlOutStr.WriteText(rlDTAlineasPlanVacunacionC01.CodigoLineas);
                            xlOutStr.WriteText(clSeparador);
                            xlOutStr.WriteText(format(rlDTAlineasPlanVacunacionC01.NumLinea));
                            xlOutStr.WriteText(clSeparador);
                            xlOutStr.WriteText(rlDTAlineasPlanVacunacionC01.ClienteAVacunar);
                            xlOutStr.WriteText(clSeparador);
                            xlOutStr.WriteText(rlDTAlineasPlanVacunacionC01.CodigoOtros);
                            xlOutStr.WriteText(clSeparador);
                            xlOutStr.WriteText(rlDTAlineasPlanVacunacionC01.CodigoVacuna);
                            xlOutStr.WriteText(clSeparador);
                            xlOutStr.WriteText(format(rlDTAlineasPlanVacunacionC01.FechaProximaVacunacion));
                            xlOutStr.WriteText(clSeparador);
                            xlOutStr.WriteText(format(rlDTAlineasPlanVacunacionC01.FechaVacunacion));
                            xlOutStr.WriteText();

                        until rlDTAlineasPlanVacunacionC01.Next() = 0;
                    end;

                until rlDTACabeceraPlanVacunacionC01.Next() = 0;
                TempLDTAConfiguracionC01.MiBlob.CreateInStream(xlInStr);
                xlInStr.ReadText(xlLinea);
                // descargamos fichero
                xlNombreFichero := 'Pedido.csv';
                if not DownloadFromStream(xlInStr, '', '', '', xlNombreFichero) then begin
                    Error('No se ha podido descargar el fichero');
                end;

            end;
        end else begin
            Error('proceso cancelado por el usuario');
        end;
    end;

    local procedure ImportarVacunasF()
    var

        xlInStr: Instream;
        xlLinea: Text;
        rlDTACabeceraPlanVacunacionC01: Record DTACabeceraPlanVacunacionC01;
        TempLDTAConfiguracionC01: Record DTAConfiguracionC01 temporary;
        clSeparador: label '·', Locked = true;
        rlDTALineasPlanVacunacionC01: Record DTALineasPlanVacunacionC01;
        xlFEcha: Date;
        xlInt: Integer;
    begin
        TempLDTAConfiguracionC01.MiBlob.CreateInStream(xlInStr, TextEncoding::Windows);
        if UploadIntoStream('', xlInStr) then begin
            while not xlInStr.EOS do begin //preguntamos si estamos al final del STREAM
                xlInStr.ReadText(xlLinea);
                Message(xlLinea);
                //insertar cabecera o lineas en funcion del primer caracter de la linea
                case true of
                    xlLinea[1] = 'C':
                        //alta cabecera
                        begin
                            rlDTACabeceraPlanVacunacionC01.Init();
                            rlDTACabeceraPlanVacunacionC01.Validate(CodigoCabecera, xlLinea.Split(clSeparador).Get(3));
                            rlDTACabeceraPlanVacunacionC01.Insert(true);
                            // en el mismo orden que exportamos
                            rlDTACabeceraPlanVacunacionC01.Validate(Descripcion, xlLinea.Split(clSeparador).Get(4));
                            if Evaluate(xlFEcha, xlLinea.Split(clSeparador).Get(6)) then begin
                                rlDTACabeceraPlanVacunacionC01.Validate(FechaInicioVacunacion, xlFEcha);
                            end;
                            rlDTACabeceraPlanVacunacionC01.Validate(EmpresaVacunadora, xlLinea.Split(clSeparador).Get(5));

                            rlDTACabeceraPlanVacunacionC01.Modify(true);
                        end;
                    xlLinea[1] = 'L':
                        //alta linea
                        begin
                            rlDTALineasPlanVacunacionC01.Init();
                            rlDTALineasPlanVacunacionC01.Validate(CodigoLineas, xlLinea.Split(clSeparador).Get(2));
                            if evaluate(xlInt, xlLinea.Split(clSeparador).Get(3)) then begin
                                rlDTALineasPlanVacunacionC01.Validate(NumLinea, xlInt);

                            end;
                            rlDTALineasPlanVacunacionC01.Insert(true);
                            rlDTALineasPlanVacunacionC01.Validate(ClienteAVacunar, xlLinea.Split(clSeparador).Get(4));
                            rlDTALineasPlanVacunacionC01.Validate(CodigoOtros, xlLinea.Split(clSeparador).Get(5));
                            rlDTALineasPlanVacunacionC01.Validate(CodigoVacuna, xlLinea.Split(clSeparador).Get(6));
                            if Evaluate(xlFEcha, xlLinea.Split(clSeparador).Get(7)) then begin
                                rlDTALineasPlanVacunacionC01.Validate(FechaProximaVacunacion, xlFEcha);
                            end;
                            if Evaluate(xlFEcha, xlLinea.Split(clSeparador).Get(8)) then begin
                                rlDTALineasPlanVacunacionC01.Validate(FechaVacunacion, xlFEcha);
                            end;
                            rlDTALineasPlanVacunacionC01.Modify(true);
                        end;
                    else
                        Error('Tipo de linea %1 no reconocido', xlLinea[1]);
                end;
            end;
        end else begin
            Error('no se a podido importar el fichero');
        end;
    end;

    procedure AccionAceptadaF(pAction: Action) returnValue: Boolean
    begin
        returnValue := pAction in [Action::LookupOK, Action::OK, Action::Yes];
    end;

    var

        xPassword: Text;
        xNumIteraciones: Integer;
}
