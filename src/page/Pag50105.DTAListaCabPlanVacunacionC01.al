page 50105 "DTAListaCabPlanVacunacionC01"
{
    ApplicationArea = All;
    Caption = 'Lista Cab. Plan Vacunación';
    PageType = List;
    SourceTable = DTACabeceraPlanVacunacionC01;
    UsageCategory = Lists;
    CardPageId = DTAFichaCabPlanVacunaC01;
    Editable = false;
    InsertAllowed = false;
    DeleteAllowed = false;
    ModifyAllowed = false;


    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Cod; Rec.CodigoCabecera)
                {
                    ToolTip = 'Specifies the value of the Código field.';
                    ApplicationArea = All;
                }
                field(Descripcion; Rec.Descripcion)
                {
                    ToolTip = 'Specifies the value of the Descripción field.';
                    ApplicationArea = All;
                }
                field(FechaInicioVacunacion; Rec.FechaInicioVacunacion)
                {
                    ToolTip = 'Specifies the value of the Fecha Inicio Vacunación field.';
                    ApplicationArea = All;
                }
                field(EmpresaVacunadora; Rec.EmpresaVacunadora)
                {
                    ToolTip = 'Specifies the value of the Empresa Vacunadora field.';
                    ApplicationArea = All;
                }
                field(NombreEmpresaVacunadora; rec.NombreEmpresaVacunadoraF())
                {
                    ApplicationArea = All;
                    Caption = 'Nombre Empresa Vacunadora';
                    ToolTip = 'Aparecerá el nombre del proveedor que realizará la vacunación';
                }

            }
        }
    }
    procedure GetSelectionFilterF(var prDTACabeceraPlanVacunacionC01: record DTACabeceraPlanVacunacionC01)

    begin
        CurrPage.SetSelectionFilter(prDTACabeceraPlanVacunacionC01);
    end;
}
