page 50101 "DTAListadeVariosC01"
{
    ApplicationArea = All;
    Caption = 'Lista de tabla Varios';
    AdditionalSearchTerms = 'curso 01';
    PageType = List;
    SourceTable = DTAVariosC01;
    UsageCategory = Lists;
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    DeleteAllowed = false;
    CardPageId = DTAFichaDeVariosC01;


    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Codigo; Rec.Codigo)
                {
                    ToolTip = 'Specifies the value of the Código field.';
                    ApplicationArea = All;

                }
                field(Tipo; Rec.Tipo)
                {
                    ToolTip = 'Specifies the value of the Tipo field.';
                    ApplicationArea = All;
                }
                field(Descripcion; Rec.Descripcion)
                {
                    ToolTip = 'Specifies the value of the Descripción field.';
                    ApplicationArea = All;
                }
                field(FechaSegundaDosis; Rec.FechaSegundaDosis)
                {
                    ToolTip = 'especifica la fecha de la segunda dosis';
                    ApplicationArea = All;
                }

                field(Bloqueado; Rec.Bloqueado)
                {
                    ToolTip = 'Specifies the value of the MyField field.';
                    ApplicationArea = All;
                }

            }
        }
    }
}
