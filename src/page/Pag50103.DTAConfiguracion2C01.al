page 50103 "DTAConfiguracion2C01"
{
    ApplicationArea = All;
    Caption = 'Configuracion 2 Lista';
    PageType = List;
    SourceTable = DTAConfiguracionC01;
    UsageCategory = Lists;
    ShowFilter = true;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Id; Rec.Id)
                {
                    ToolTip = 'Specifies the value of the Id. de registro field.';
                    ApplicationArea = All;
                }
                field(CodCteWeb; Rec.CodCteWeb)
                {
                    ToolTip = 'Configure la app de forma correcta. Asegurese que asigna el valor del campo de cliente web';
                    ApplicationArea = All;
                }
                field(TextoRegistro; Rec.TextoRegistro)
                {
                    ToolTip = 'Specifies the value of the Texto Registro field.';
                    ApplicationArea = All;
                }
                field(TipoDoc; Rec.TipoDoc)
                {
                    ToolTip = 'Specifies the value of the Tipo de documento field.';
                    ApplicationArea = All;
                }
                field(NombreCliente; Rec.NombreCliente)
                {
                    ToolTip = 'Campo calculado que nos muestra nombre del cliente';
                    ApplicationArea = All;
                }
                field(TipoTabla; Rec.TipoTabla)
                {
                    ToolTip = 'Specifies the value of the Tipo de documento field.';
                    ApplicationArea = All;
                }
                field(codTabla; Rec.codTabla)
                {
                    ToolTip = 'Specifies the value of the Tipo de documento field.';
                    ApplicationArea = All;
                }
                field(ColorFondo; Rec.ColorFondo)
                {
                    ToolTip = 'Specifies the value of the Tipo de documento field.';
                    ApplicationArea = All;
                }
                field(ColorLetra; Rec.ColorLetra)
                {
                    ToolTip = 'Specifies the value of the Tipo de documento field.';
                    ApplicationArea = All;
                }
                field(UnidadesDisponibles; Rec.UnidadesDisponibles)
                {
                    ToolTip = 'Total de unidades vendidas';
                    ApplicationArea = All;
                }
            }
        }
    }
}
