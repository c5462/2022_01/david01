report 50101 "DTAListadoProyectosC01"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    RDLCLayout = 'listadoproyectos.rdlc';
    DefaultLayout = RDLC;
    Caption = './Layaout/Listado Proyectos';

    dataset
    {
        dataitem(Proyectos; Job)
        {
            // DataItemTableView = sorting("No.");
            RequestFilterFields = "No.", "Creation Date", "Status";
            column(Estado; Status)
            {

            }
            column(NombreCliente; "Bill-to Name")
            {
            }
            column(FechaCreacion; Format("Creation Date"))
            {
            }
            column(NumProyecto; "No.")
            {
            }
            column(CodCliente; "Bill-to Customer No.")
            {
            }
            column(Descripcion; "Description")
            {
            }
            column(xColor; xColor)
            {

            }
            dataitem(TareasProyecto; "Job Task")
            {

                RequestFilterFields = "job task no.", "job task type";
                DataItemLink = "job no." = field("no.");
                column(NumTarea; "Job Task No.")
                {
                }
                column(TipoTarea; "Job Task Type")
                {
                }
                column(DescripcionTarea; "Description")
                {
                }
                column(FechaInicioTarea; format("Start Date"))
                {
                }
                column(FechaFinTarea; format("End Date"))
                {
                }
                dataitem(LineasPlanificacion; "Job Planning Line")
                {
                    RequestFilterFields = "No.", "planning date", "type";
                    DataItemLink = "job no." = field("job no."), "job task no." = field("job task no.");
                    column(NumLinea; "No.")
                    {
                    }
                    column(NumLineaPlanificacion; "Line No.")
                    {
                    }
                    column(DescripcionLinea; "Description")
                    {
                    }
                    column(Cantidad; "Quantity")
                    {
                    }
                    column(FechaPlanificacion; format("Planning Date"))
                    {
                    }
                    column(ImporteLinea; "Line Amount")
                    {
                    }
                    column(Tipo; "Type")
                    {
                    }
                    dataitem(MovimientosProyectos; "Job Ledger Entry")
                    {

                        DataItemLink = "Entry No." = field("Job Ledger Entry No.");
                        column(EntryType_MovimientosProyectos; "Entry Type")
                        {
                        }
                        column(EntryNo_MovimientosProyectos; "Entry No.")
                        {
                        }
                        column(LineType_MovimientosProyectos; "Line Type")
                        {
                        }
                        column(LedgerEntryType_MovimientosProyectos; "Ledger Entry Type")
                        {
                        }
                        column(DocumentNo_MovimientosProyectos; "Document No.")
                        {
                        }
                        column(DocumentDate_MovimientosProyectos; "Document Date")
                        {
                        }
                        column(Type_MovimientosProyectos; "Type")
                        {
                        }
                    }
                }
            }
            trigger OnAfterGetRecord()
            begin
                case Proyectos.Status of
                    proyectos.Status::Completed:
                        begin
                            xColor := 'Green'
                        end;
                    proyectos.Status::Open:
                        begin
                            xColor := 'Blue'
                        end;
                    proyectos.Status::Planning:
                        begin
                            xColor := 'Yellow'
                        end;
                    proyectos.Status::Quote:
                        begin
                            xColor := 'Red'
                        end;

                end;
            end;
        }



    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    // field(Name; SourceExpression)
                    // {
                    //     ApplicationArea = All;

                    // }
                }
            }
        }


    }

    var
        xColor: Text;
}