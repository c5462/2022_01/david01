/// <summary>
/// report de factura de venta
/// </summary>
report 50102 "DTAFacturaVentaC01"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    DefaultLayout = RDLC;
    RDLCLayout = './Layaout/FacturaVenta1.rdlc';

    dataset
    {
        dataitem(CabecerasFacturaVenta; "Sales Invoice Header")
        {
            column(NumFactura; "No.")
            {

            }
            column(xcustAddr1; xCustAddr[1])
            {

            }
            column(xcustAddr2; xCustAddr[2])
            {

            }
            dataitem(Integer; Integer)
            {

                column(Number; Number)
                {

                }
                dataitem(LineasFacturaVenta; "Sales Invoice Line")
                {
                    //DataItemLink = "Document No." = field("No.");
                    column(NumLineaFactura; "Line No.")
                    {

                    }
                    trigger OnPreDataItem()
                    begin
                        LineasFacturaVenta.SetRange("Document No.", CabecerasFacturaVenta."No.");

                    end;

                }
                trigger OnPreDataItem()
                begin
                    Integer.SetRange(Number, 1, xCopias);
                end;
            }
            trigger OnAfterGetRecord()
            begin
                cuFormatAdress.SalesInvSellTo(xCustAddr, CabecerasFacturaVenta);
                cuFormatAdress.SalesInvShipTo(xShipAddr, xCustAddr, CabecerasFacturaVenta);
                rCompanyInfo.get();
                cuFormatAdress.Company(xCompanyAddr, rCompanyInfo);

            end;
        }
    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    field(xCopias; xCopias)
                    {
                        ApplicationArea = All;
                        Caption = 'copias';

                    }
                }
            }
        }

        actions
        {
            area(processing)
            {

            }
        }
    }

    var
        rCompanyInfo: Record "Company Information";
        cuFormatAdress: Codeunit "Format Address";
#pragma warning disable AA0204
        xCopias: Integer;
#pragma warning restore AA0204
        xCompanyAddr: array[8] of Text;
        xCustAddr: array[8] of Text;
        xShipAddr: array[8] of Text;



}