/// <summary>
/// Report de etiquetas
/// </summary>
report 50105 "DTAEtiquetaC01"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    DefaultLayout = RDLC;
    RDLCLayout = './Layaout/etiqueta.rdlc';
    //UseRequestPage = false;

    dataset
    {
        dataitem("sales shipment line"; "Sales Shipment Line")
        {

            column(DocumentNo_salesshipmentline; "Document No.")
            {
            }
            column(No_salesshipmentline; "No.")
            {
            }
            column(Description_salesshipmentline; Description)
            {
            }
            dataitem(Integer; Integer)
            {

                column(Number_Integer; Number)
                {
                }
                trigger OnPreDataItem()
                var

                begin
                    Integer.SetRange(Number, 1, "sales shipment line".Quantity);
                end;
            }
        }
    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {

                }
            }
        }

        actions
        {
            area(processing)
            {

            }
        }
    }

    var

}