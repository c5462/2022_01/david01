/// <summary>
/// Report documento de venta
/// </summary>
report 50103 "DTADocumentoVentaC01"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    //descomentar para report builder
    //DefaultLayout = RDLC;
    //RDLCLayout = './Layaout/documentoventa.rdlc';

    //para hacer informe con Word en vez report builder
    DefaultLayout = Word;
    WordLayout = './Layaout/ducomentoventa.docx';

    dataset
    {
        dataitem(SalesHeader; "Sales Header")
        {
            RequestFilterFields = "No.", "Document Type";
            column(No_SalesHeader; "No.")
            {
            }
            column(SelltoCustomerNo_SalesHeader; "Sell-to Customer No.")
            {
            }
            column(PostingDate_SalesHeader; "Posting Date")
            {
            }
            column(xDocumentLabel; xDocumentLabel)
            {

            }
            column(xCustAddr1; xCustAddr[1])
            {

            }
            column(xCustAddr2; xCustAddr[2])
            {

            }
            column(xCustAddr3; xCustAddr[3])
            {

            }
            column(xCustAddr4; xCustAddr[4])
            {

            }
            column(xCustAddr5; xCustAddr[5])
            {

            }
            column(xCustAddr6; xCustAddr[6])
            {

            }
            column(xCustAddr7; xCustAddr[7])
            {

            }
            column(xCompany8; xCustAddr[8])
            {

            }
            column(xCompanyaddr1; xCompanyAddr[1])
            {

            }
            column(xCompanyaddr2; xCompanyAddr[2])
            {

            }
            column(xCompanyaddr3; xCompanyAddr[3])
            {

            }
            column(xCompanyaddr4; xCompanyAddr[4])
            {

            }
            column(xCompanyaddr5; xCompanyAddr[5])
            {

            }
            column(xCompanyaddr6; xCompanyAddr[6])
            {

            }
            column(xCompanyaddr7; xCompanyAddr[7])
            {

            }
            column(xCompanyaddr8; xCompanyAddr[8])
            {

            }
            column(Logo; rcompanyInfo.Picture)
            {

            }
            column(xTotalIVA11; xTotalIVA[1, 1])
            {

            }
            column(xTotalIVA21; xTotalIVA[2, 1])
            {

            }
            column(xTotalIVA31; xTotalIVA[3, 1])
            {

            }
            column(xTotalIVA41; xTotalIVA[4, 1])
            {

            }
            column(xTotalIVA12; xTotalIVA[1, 2])
            {

            }
            column(xTotalIVA22; xTotalIVA[2, 2])
            {

            }
            column(xTotalIVA32; xTotalIVA[3, 2])
            {

            }
            column(xTotalIVA42; xTotalIVA[4, 2])
            {

            }
            column(xTotalIVA13; xTotalIVA[1, 3])
            {

            }
            column(xTotalIVA23; xTotalIVA[2, 3])
            {

            }
            column(xTotalIVA33; xTotalIVA[3, 3])
            {

            }
            column(xTotalIVA43; xTotalIVA[4, 3])
            {

            }
            column(xTotalIVA14; xTotalIVA[1, 4])
            {

            }
            column(xTotalIVA24; xTotalIVA[2, 4])
            {

            }
            column(xTotalIVA34; xTotalIVA[3, 4])
            {

            }
            column(xTotalIVA44; xTotalIVA[4, 4])
            {

            }

            dataitem(copias; Integer)
            {
                column(Number_copias; Number)
                {
                }



                dataitem("Sales Line"; "Sales Line")
                {
                    // DataItemLink = "Document No." = field("No."), "Document Type" = field("Document Type");
                    column(No_SalesLine; "No.")
                    {
                    }
                    column(Description_SalesLine; Description)
                    {
                    }
                    column(Quantity_SalesLine; Quantity)
                    {
                    }
                    column(UnitPrice_SalesLine; "Unit Price")
                    {
                    }
                    column(LineDiscount_SalesLine; "Line Discount %")
                    {
                    }
                    column(Amount_SalesLine; Amount)
                    {
                    }
                    column(Line_No_; "Line No.")
                    {

                    }
                    dataitem("Extended Text Line"; "Extended Text Line")
                    { //sacar comentarios de cada producto si los hubiera
                        DataItemLink = "No." = field("No.");
                        column(text_extendedTextline; "Text")
                        {

                        }

                    }
#pragma warning disable AL0432
                    dataitem("Item Cross Reference"; "Item Cross Reference")
#pragma warning restore AL0432
                    {

                        DataItemLink = "Item No." = field("No.");
                        DataItemTableView = where("Cross-reference type" = const(Customer));
                        column(CrossReferenceNo_ItemCrossReference; "Cross-Reference No.")
                        {
                        }
                        column(Description_ItemCrossReference; Description)
                        {
                        }
                    }
                    trigger OnPreDataItem()
                    begin
                        "sales line".SetRange("Document No.", SalesHeader."No.");
                        "sales line".SetRange("Document Type", SalesHeader."Document Type");
                    end;


                }
                trigger OnPreDataItem()
                begin
                    // if xCopias = 0 then begin
                    //     xCopias := 1;
                    // end;
                    copias.SetRange(Number, 0, xCopias);
                end;
            }
            trigger OnAfterGetRecord()

            begin
                case SalesHeader."Document Type" of
                    salesheader."Document Type"::Quote:
                        begin
                            cuFormatAddress.SalesHeaderSellTo(xCustAddr, SalesHeader);
                            xDocumentLabel := 'Nº oferta';

                        end;
                    salesheader."Document Type"::Order:
                        begin
                            cuFormatAddress.SalesHeaderSellTo(xCustAddr, SalesHeader);
                            xDocumentLabel := 'nº pedido';

                        end;
                    salesheader."Document Type"::Invoice:
                        begin
                            cuFormatAddress.SalesHeaderSellTo(xCustAddr, SalesHeader);
                            xDocumentLabel := 'Nº factura';

                        end;
                end;
                CalculoIVAF();
            end;

            trigger OnPreDataItem()
            begin
                SalesHeader.SetRange("Document Type", rSalesHeader."Document Type");
                SalesHeader.SetFilter("No.", xDocumentNo);
            end;

        }

    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    field(rSalesHeader; rSalesHeader."Document Type")
                    {
                        ApplicationArea = All;

                    }
                    field(xDocumentNo; xDocumentNo)
                    {
                        ApplicationArea = all;
                        Caption = 'nombre documento';
                        trigger OnLookup(var mytext: Text): Boolean
                        var
                            plSalesHeader: Page "Sales List";
                        begin
                            rSalesHeader.SetRange("Document Type", rSalesHeader."Document Type");// tengo el campo como valor pero lo necesito como filtro
                            plSalesHeader.SetTableView(rSalesHeader);//aplico filtros de tabla a pagina
                            plSalesHeader.LookupMode := true;// pongo la pagina en modo lookup
                            if plSalesHeader.RunModal() = Action::LookupOK then begin //Ejecuto pagina y controlo la accion del usuario
                                plSalesHeader.SetSelectionFilter(rSalesHeader);// aplico seleccion del usuario de la pagina a la tabla
                                if rSalesHeader.FindSet() then begin // busqueda
                                    repeat
                                        xDocumentNo += rSalesHeader."No." + '|';
                                    // elaboracion de filtro
                                    until rSalesHeader.Next() = 0;
                                end;
                                xDocumentNo := DelChr(xDocumentNo, '>', '|'); // limpio concatenacion final
                            end;
                        end;
                    }
                    field(Copias1; xCopias)
                    {
                        ApplicationArea = all;
                        Caption = 'copias';
                    }
                }
            }
        }


    }

    trigger OnPreReport()

    begin
        rCompanyInfo.get();
        rCompanyInfo.CalcFields(Picture);
        cuFormatAddress.Company(xCompanyAddr, rCompanyInfo);

    end;

    /// <summary>
    /// Funcion Calcular totales de IVA
    /// </summary>
    procedure CalculoIVAF()
    var
        rlSalesLine: Record "Sales Line";
#pragma warning disable AA0073
        rlTempVATAmount: Record "VAT Amount Line" temporary;
#pragma warning restore AA0073
        i: Integer;
    begin
        rlSalesLine.SetRange("Document No.", SalesHeader."No.");
        rlSalesLine.SetRange("Document Type", SalesHeader."Document Type");
        if rlSalesLine.FindSet() then begin
            repeat
                rlTempVATAmount.SetRange("VAT Identifier", rlSalesLine."VAT Identifier");
                if rlTempVATAmount.FindFirst() then begin
                    //modificar
                    rlTempVATAmount."VAT Base" += rlSalesLine.Amount;
                    rlTempVATAmount."VAT Amount" += rlSalesLine."Amount Including VAT" - rlSalesLine.Amount;
                    rlTempVATAmount."Amount Including VAT" += rlSalesLine."Amount Including VAT";
                    rlTempVATAmount.Modify(false);
                end else begin
                    //insert
                    rlTempVATAmount.Init();
                    rlTempVATAmount."VAT Base" := rlSalesLine.Amount;
                    rlTempVATAmount."VAT %" := rlSalesLine."VAT %";
                    rlTempVATAmount."VAT Amount" := rlSalesLine."Amount Including VAT" - rlSalesLine.Amount;
                    rlTempVATAmount."Amount Including VAT" := rlSalesLine."Amount Including VAT";
                    rlTempVATAmount."VAT Identifier" := rlSalesLine."VAT Identifier";
                    rlTempVATAmount.Insert(false);
                end;
            until rlSalesLine.Next() = 0;
        end;
        clear(xTotalIVA); // limpiamos el array
        rlTempVATAmount.FindSet();
        for i := 1 to 4 do begin
            xTotalIVA[1, i] := format(rlTempVATAmount."VAT Base");
            xTotalIVA[2, i] := format(rlTempVATAmount."VAT %");
            xTotalIVA[3, i] := format(rlTempVATAmount."VAT amount");
            xTotalIVA[4, i] := format(rlTempVATAmount."Amount Including VAT");
            // para que salga del for igualar i con algo mayor de 4
            if rlTempVATAmount.Next() = 0 then begin
                i := 1000;
            end;
        end;
    end;

    var

        rCompanyInfo: Record "Company Information";
#pragma warning disable AA0204
        rSalesHeader: Record "Sales Header";
#pragma warning restore AA0204
        cuFormatAddress: Codeunit "Format Address";
        xCopias: Integer;
        xCompanyAddr: array[8] of Text;

        xCustAddr: array[8] of Text;
        xDocumentLabel: Text;


#pragma warning disable AA0204
        xDocumentNo: Text;
#pragma warning restore AA0204
        xTotalIVA: array[4, 4] of Text;
}