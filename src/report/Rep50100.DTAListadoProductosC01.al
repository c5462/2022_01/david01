/// <summary>
/// informe listado de productos
/// </summary>
report 50100 "DTAListadoProductosC01"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    DefaultLayout = RDLC;
    RDLCLayout = './Layaout/ListadoProductos.rdlc';
    AdditionalSearchTerms = 'Listado Productos Ventas/Compras';
    Caption = 'Listado Productos Ventas/Compras';

    dataset
    {
        dataitem(Productos; Item)
        {
            RequestFilterFields = "No.", "Item Category Code";
            column(No_Productos; "No.")
            {
            }
            column(Description_Productos; Description)
            {
            }
            column(ItemCategoryCode_Productos; "Item Category Code")
            {
            }
            dataitem(FacturaVenta; "Sales Invoice Line")
            {
                DataItemLink = "No." = field("No.");
                RequestFilterFields = "No.", "Item Category Code";
                column(Quantity_FacturaVenta; Quantity)
                {
                }
                column(UnitPrice_FacturaVenta; "Unit Price")
                {
                }
                column(AmountIncludingVAT_FacturaVenta; "Amount Including VAT")
                {
                }
                column(PostingDate_FacturaVenta; "Posting Date")
                {
                }
                column(NombreCliente; rCustomer.Name)
                {

                }
                column(NoCliente; rCustomer."No.")
                {

                }
                column(DocumentNo_FacturaVenta; "Document No.")
                {
                }

                // trigger OnPreDataItem()
                // begin
                //     FacturaVenta.SetRange("No.", Productos."No.");
                // end;

                trigger OnAfterGetRecord()

                begin
                    if not rCustomer.Get(FacturaVenta."Sell-to Customer No.") then begin
                        Clear(rCustomer);
                    end;
                end;
            }
            dataitem(FacturaCompra; "Purch. Inv. Line")
            {
                DataItemLink = "No." = field("No.");
                RequestFilterFields = "No.", "Item Category Code";
                column(Quantity_FacturaCompra; Quantity)
                {
                }
                column(UnitCost_FacturaCompra; "Unit Cost")
                {
                }
                column(AmountIncludingVAT_FacturaCompra; "Amount Including VAT")
                {
                }
                column(PostingDate_FacturaCompra; "Posting Date")
                {
                }
                column(NombreProveedor; rVendor.Name)
                {

                }
                column(NoProveedor; rvendor."No.")
                {

                }
                column(DocumentNo_FacturaCompra; "Document No.")
                {
                }

                // trigger OnPreDataItem()
                // begin
                //     FacturaCompra.SetRange("No.", Productos."No.");
                // end;

                trigger OnAfterGetRecord()

                begin
                    if not rVendor.Get(FacturaCompra."Buy-from Vendor No.") then begin
                        Clear(rVendor);
                    end;
                end;
            }

        }

    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    // field(Name; SourceExpression)
                    // {
                    //     ApplicationArea = All;

                    // }
                }
            }
        }


    }


    var
        rCustomer: Record Customer;
        rVendor: Record Vendor;

}