pageextension 50102 "DTACustomerListExtC01" extends "Customer List"
{
    actions
    {
        addfirst(General)
        {
            action(DTASwitchVariableC01)
            {
                ApplicationArea = all;
                Caption = 'Prueba de variables globales a BC';
                trigger OnAction()
                var

                    clMensaje: Label 'El valor es %1. Lo cambiamos a %2';

                begin
                    Message(clMensaje, cuDTAFuncionesAppC01.GetSwitchF(), not cuDTAFuncionesAppC01.GetSwitchF());
                    cuDTAFuncionesAppC01.SetSwitchF(not cuDTAFuncionesAppC01.GetSwitchF());
                end;
            }
            action(DTAPruebaCuCapturaErroresC01)
            {
                ApplicationArea = all;
                Caption = 'Prueba Codeunit de captura de errores';
                trigger OnAction()
                var
                    culDTACapturarErroresC01: Codeunit DTACapturarErroresC01;
                begin
                    if culDTACapturarErroresC01.Run() then begin

                        Message('El botón dice que la función se ha ejecutado correctamente');
                    end else begin
                        Message('No se a podido ejecutar la Cu. Motivo %1', GetLastErrorText());
                    end;
                end;
            }
            action(DTAPrueba02CuCapturaErroresC01)
            {
                ApplicationArea = all;
                Caption = 'Prueba 02 Codeunit de captura de errores';
                trigger OnAction()
                var
                    culDTACapturarErroresC01: Codeunit DTACapturarErroresC01;
                    i: Integer;
                    xlCodProveedor: Code[20];
                begin
                    xlCodProveedor := '10000';
                    for i := 1 to 5 do begin
                        if culDTACapturarErroresC01.FuncionParaCapturarErrorF(xlCodProveedor) then begin

                            Message('Se ha creado el proveedor %1', xlCodProveedor);
                        end;
                        xlCodProveedor := IncStr(xlCodProveedor);
                    end;
                end;
            }
        }
        addlast(History)
        {
            action(DTAMostraFacturasyAbonosC01)
            {
                Caption = 'Mostrar Facturas y Abonos';
                ApplicationArea = all;

                trigger OnAction()

                begin
                    FrasyAbonosF();
                end;
            }
            action(DTAExportarClientesC01)
            {
                ApplicationArea = all;
                caption = 'Exportar Clientes';
                trigger OnAction()
                begin
                    ExportarClientesF();
                end;
            }
            action(DTASumatorio01C01)
            {
                ApplicationArea = All;
                Caption = 'sumatorio01';
                trigger OnAction()
                var
                    rlDetailedCustLedgEntry: Record "Detailed Cust. Ledg. Entry";
                    xlTotal: Decimal;
                begin
                    rlDetailedCustLedgEntry.SetRange("Customer No.", Rec."No.");
                    if rlDetailedCustLedgEntry.FindSet(false) then begin
                        repeat
                            xlTotal += rlDetailedCustLedgEntry.Amount;
                        until rlDetailedCustLedgEntry.Next() = 0;
                    end;
                    Message('total = %1', xlTotal);
                end;
            }
            action(DTASumatorio02C01)
            {
                ApplicationArea = All;
                Caption = 'sumatorio02';
                trigger OnAction()
                var
                    rlDetailedCustLedgEntry: Record "Detailed Cust. Ledg. Entry";
                    xlTotal: Decimal;
                begin
                    rlDetailedCustLedgEntry.SetRange("Customer No.", Rec."No.");
                    rlDetailedCustLedgEntry.CalcSums(Amount);
                    xlTotal := rlDetailedCustLedgEntry.Amount;
                    Message('total = %1', xlTotal);
                end;
            }
            action(DTASumatorio03C01)
            {
                ApplicationArea = All;
                Caption = 'sumatorio03';
                trigger OnAction()
                var
                    rlCustLedgerEntry: Record "Cust. Ledger Entry";
                    xlTotal: Decimal;
                begin
                    rlCustLedgerEntry.SetRange("Customer No.", Rec."No.");
                    rlCustLedgerEntry.SetLoadFields(amount);
                    rlCustLedgerEntry.setautoCalcFields(Amount);
                    if rlCustLedgerEntry.FindSet(false) then begin
                        repeat
                            xlTotal += rlCustLedgerEntry.Amount;
                        until rlCustLedgerEntry.Next() = 0;
                    end;
                    Message('total = %1', xlTotal);
                end;
            }
            action(DTASumatorio04C01)
            {
                ApplicationArea = All;
                Caption = 'sumatorio04';
                trigger OnAction()
                var
                    rlCustLedgerEntry: Record "Cust. Ledger Entry";
                    xlTotal: Decimal;
                begin
                    rlCustLedgerEntry.SetRange("Customer No.", Rec."No.");
                    rlCustLedgerEntry.CalcSums(Amount);
                    xlTotal := rlCustLedgerEntry.Amount;
                    Message('total = %1', xlTotal);
                end;
            }
            action(DTAExportarExcelC01)
            {
                ApplicationArea = All;
                Caption = 'exportar Excel';
                Image = Excel;

                trigger OnAction()
                var
                    TempLExcelBuffer: Record "Excel Buffer" temporary;
                    rlCustomer: Record Customer;
                    xlFila: Integer;
                    xlCol: Integer;
                begin
                    TempLExcelBuffer.CreateNewBook('Clientes'); // creamos libro
                                                                //rellenar cabecera

                    AsignaCeldaF(1, 1, rec.FieldCaption("No."), true, true, TempLExcelBuffer);
                    AsignaCeldaF(1, 2, rec.FieldCaption(Name), true, true, TempLExcelBuffer);
                    AsignaCeldaF(1, 3, rec.FieldCaption("Responsibility Center"), true, true, TempLExcelBuffer);
                    AsignaCeldaF(1, 4, rec.FieldCaption("Location Code"), true, true, TempLExcelBuffer);
                    AsignaCeldaF(1, 5, rec.FieldCaption("Phone No."), true, true, TempLExcelBuffer);
                    AsignaCeldaF(1, 6, rec.FieldCaption(Contact), true, true, TempLExcelBuffer);
                    AsignaCeldaF(1, 7, rec.FieldCaption(Balance), true, true, TempLExcelBuffer);
                    AsignaCeldaF(1, 8, rec.FieldCaption("Balance Due (LCY)"), true, true, TempLExcelBuffer);
                    AsignaCeldaF(1, 9, rec.FieldCaption("Sales (LCY)"), true, true, TempLExcelBuffer);

                    //rellenar las celdas
                    CurrPage.SetSelectionFilter(rlCustomer);
                    rlCustomer.SetAutoCalcFields(Balance, "Balance Due (LCY)", "Sales (LCY)");
                    rlCustomer.setLoadFields("No.", Name, "Responsibility Center", "Location Code", "Phone No.", Contact, Balance, "Balance Due (LCY)", "Sales (LCY)");
                    if rlCustomer.FindSet(false) then begin
                        xlFila := 2;
                        repeat
                            xlCol := 1;
                            AsignaCeldaF(xlFila, xlCol, rlCustomer."No.", false, false, TempLExcelBuffer);
                            xlcol += 1;
                            AsignaCeldaF(xlFila, xlCol, rlCustomer.Name, false, false, TempLExcelBuffer);
                            xlcol += 1;
                            AsignaCeldaF(xlFila, xlCol, rlCustomer."Responsibility Center", false, false, TempLExcelBuffer);
                            xlcol += 1;
                            AsignaCeldaF(xlFila, xlCol, rlCustomer."Location Code", false, false, TempLExcelBuffer);
                            xlcol += 1;
                            AsignaCeldaF(xlFila, xlCol, rlCustomer."Phone No.", false, false, TempLExcelBuffer);
                            xlcol += 1;
                            AsignaCeldaF(xlFila, xlCol, rlCustomer.Contact, false, false, TempLExcelBuffer);
                            xlcol += 1;
                            AsignaCeldaF(xlFila, xlCol, format(rlCustomer.Balance), false, false, TempLExcelBuffer);
                            xlcol += 1;
                            AsignaCeldaF(xlFila, xlCol, format(rlCustomer."Balance Due (LCY)"), false, false, TempLExcelBuffer);
                            xlcol += 1;
                            AsignaCeldaF(xlFila, xlCol, format(rlCustomer."Sales (LCY)"), false, false, TempLExcelBuffer);
                            xlFila += 1;
                        until rlCustomer.Next() = 0;
                    end;

                    TempLExcelBuffer.WriteSheet('', '', ''); // preparamos para poder escribir en la hoja
                    TempLExcelBuffer.CloseBook(); // cierra el libro
                    TempLExcelBuffer.OpenExcel(); // nos lo abre en excel si lo tenemos instalado en PC
                end;
            }
            action(DTAImportarExcelC01)
            {
                ApplicationArea = All;
                Caption = 'Importar Excel';
                Image = Import;

                trigger OnAction()
                var
                    TempLExcelBuffer: Record "Excel Buffer" temporary;
                    xlFicheroClientes: Text;
                    xlInStr: Instream;
                    xlHoja: Text;
                    rlCustomer: Record Customer;
                begin
                    if UploadIntoStream('Seleccione el fichero excel para importar clientes', '', '|*.xls;*.xlsx', xlFicheroClientes, xlInStr) then begin
                        xlHoja := TempLExcelBuffer.SelectSheetsNameStream(xlInStr);
                        if xlHoja = '' then begin
                            Error('Proceso cancelado');
                        end;
                        TempLExcelBuffer.OpenBookStream(xlInStr, xlHoja);
                        TempLExcelBuffer.ReadSheet();
                        TempLExcelBuffer.SetFilter("Row No.", '>=2');
                        TempLExcelBuffer.SetFilter("Column No.", '<=6');
                        if TempLExcelBuffer.FindSet(false) then begin
                            repeat

                                case TempLExcelBuffer."Column No." of
                                    1:
                                        begin
                                            rlCustomer.Init();
                                            rlCustomer.Validate("No.", TempLExcelBuffer."Cell Value as Text");
                                            rlCustomer.Insert(true);
                                        end;
                                    2:
                                        begin
                                            rlCustomer.Validate(Name, TempLExcelBuffer."Cell Value as Text");
                                            rlCustomer.Modify(true);
                                        end;
                                    3:
                                        begin
                                            rlCustomer.Validate("Responsibility Center", TempLExcelBuffer."Cell Value as Text");
                                            rlCustomer.Modify(true);
                                        end;
                                    4:
                                        begin
                                            rlCustomer.Validate("Location Code", TempLExcelBuffer."Cell Value as Text");
                                            rlCustomer.Modify(true);
                                        end;
                                    5:
                                        begin
                                            rlCustomer.Validate("Phone No.", TempLExcelBuffer."Cell Value as Text");
                                            rlCustomer.Modify(true);
                                        end;
                                    6:
                                        begin
                                            rlCustomer.Validate(Contact, TempLExcelBuffer."Cell Value as Text");
                                            rlCustomer.Modify(true);
                                        end;

                                end;
                            until TempLExcelBuffer.Next() = 0;
                        end;
                    end else begin
                        Error('no se a podido cargar el fichero');
                    end;
                end;
            }
            action(DTAPaginaDeFiltrosC01)
            {
                ApplicationArea = All;
                Caption = 'Pagina de Filtros';

                trigger OnAction()
                var
                    xlPaginaFiltros: FilterPageBuilder;
                    rlCustomer: Record Customer;
                    rlCustLedgerEntry: Record "Cust. Ledger Entry";
                    rlSalesPrice: Record "Sales Price";
                    rlVendor: record vendor;
                    clClientes: Label 'Clientes';
                    clMovimientos: Label 'Movimientos';
                    clPrecios: Label 'Precios';
                    pglDTAConfiguracionC01: page DTAConfiguracionC01;
                begin
                    xlPaginaFiltros.PageCaption('Seleccione los clientes a procesar');
                    xlPaginaFiltros.AddRecord(clClientes, rlCustomer);
                    xlPaginaFiltros.AddField(clClientes, rlCustomer."No.");
                    xlPaginaFiltros.AddField(clClientes, rlCustomer.DTAFechaUltimaVacunaC01);
                    xlPaginaFiltros.AddRecord(clMovimientos, rlCustLedgerEntry);
                    xlPaginaFiltros.AddField(clMovimientos, rlCustLedgerEntry."Posting Date");
                    xlPaginaFiltros.AddField(clMovimientos, rlCustLedgerEntry."Document Type");
                    xlPaginaFiltros.AddRecord(clPrecios, rlSalesPrice);
                    xlPaginaFiltros.AddField(clPrecios, rlSalesPrice."Item No.");
                    xlPaginaFiltros.AddField(clPrecios, rlSalesPrice."Starting Date");
                    if xlPaginaFiltros.RunModal() then begin
                        rlCustomer.SetView(xlPaginaFiltros.GetView(clClientes, true));
                        //rlVendor.SetView(xlPaginaFiltros.GetView(clClientes, true));
                        if rlCustomer.FindSet(false) then begin
                            repeat
                            // aqui codigo con lo que se quiere hacer
                            until rlCustomer.Next() = 0;
                        end;
                        //para el resto de messages hacer lo mismo y quitar el message
                        Message(xlPaginaFiltros.GetView(clMovimientos, false));
                        Message(xlPaginaFiltros.GetView(clPrecios, true));
                    end else begin
                        Error('Proceso cancelado por el usuario');
                    end;

                end;
            }



        }

    }

    local procedure AsignaCeldaF(pFila: integer; pColumna: Integer; pContenidoCelda: Text; pNegrita: Boolean; pItalica: Boolean; var TempPExcelBuffer: Record "Excel Buffer" temporary)
    var
    begin
        TempPExcelBuffer.Init();
        TempPExcelBuffer.Validate("Row No.", pFila); // hago validate porque esta table contiene codigo onvalidate en esos campos
        TempPExcelBuffer.Validate("Column No.", pColumna);
        TempPExcelBuffer.Insert(true);
        TempPExcelBuffer.Validate("Cell Value as Text", pContenidoCelda);
        TempPExcelBuffer.Validate(Bold, pNegrita);
        TempPExcelBuffer.Validate(Italic, pItalica);
        TempPExcelBuffer.Modify(true);



    end;

    local procedure FrasyAbonosF()
    var
        TempLSalesInvoiceHeader: Record "Sales Invoice Header" temporary;
        rlSalesInvoiceHeader: Record "Sales Invoice Header";
        rlSalesCrMemoHeader: Record "Sales Cr.Memo Header";
    begin
        // recorremos las facturas del cliente, y las insertamos en tabla temporal
        rlSalesInvoiceHeader.SetRange("Bill-to Customer No.", rec."No.");
        //Message(rlSalesInvoiceHeader.GetFilter("Bill-to Customer No.")); // prueba propiedad getfilter
        if rlSalesInvoiceHeader.FindSet(false) then begin
            repeat
                TempLSalesInvoiceHeader.Init();
                TempLSalesInvoiceHeader.TransferFields(rlSalesInvoiceHeader);
                TempLSalesInvoiceHeader.Insert(false);
            until rlSalesInvoiceHeader.Next() = 0;
        end;

        //recorremos los abonos del clientes, y los insertamos en la tabla tempora
        rlSalesCrMemoHeader.SetRange("Bill-to Customer No.", rec."No.");
        //Message(rlSalesCrMemoHeader.GetFilters); // prueba propiedad getfilters
        if rlSalesCrMemoHeader.FindSet() then begin
            repeat
                TempLSalesInvoiceHeader.Init();
                TempLSalesInvoiceHeader.TransferFields(rlSalesCrMemoHeader);
                TempLSalesInvoiceHeader.Insert(false);
            until rlSalesCrMemoHeader.Next() = 0;
        end;
        // Mostramos la pagina con facturas y abonos

        page.Run(0, TempLSalesInvoiceHeader);

    end;

    local procedure ExportarClientesF()
    var

        TempLDTAConfiguracionC01: Record DTAConfiguracionC01 temporary;
        clSeparador: Label '·';
        xlInStr: InStream;
        xlOutStr: OutStream;
        xlLinea: Text;
        xlNombreFichero: Text;
        rlCustomer: Record Customer;
    begin
        TempLDTAConfiguracionC01.MiBlob.CreateOutStream(xloutStr, TextEncoding::Windows);
        //escribir el registro
        rlCustomer := Rec;
        rlCustomer.Find(); // estas 2 instrucciones es lo mismo que hacer un get
        rlCustomer.CalcFields("Balance (LCY)"); // para sacar el campo calculado en exportacion
        //rec.CalcFields("Balance (LCY)");
        xlOutStr.WriteText('C');
        xlOutStr.WriteText(clSeparador);
        xlOutStr.WriteText(rlCustomer."No.");
        xlOutStr.WriteText(clSeparador);
        xlOutStr.WriteText(rlCustomer.Name);
        xlOutStr.WriteText(clSeparador);
        xlOutStr.WriteText(format(rlCustomer."Balance (LCY)"));
        xlOutStr.WriteText();

        TempLDTAConfiguracionC01.MiBlob.CreateInStream(xlInStr);
        // descargamos fichero
        xlNombreFichero := 'Clientes.csv';
        if not DownloadFromStream(xlInStr, '', '', '', xlNombreFichero) then begin
            Error('No se ha podido descargar el fichero');
        end;



    end;


    var
        cuDTAFuncionesAppC01: Codeunit "DTAVblesGlobalesAppC01";


}


