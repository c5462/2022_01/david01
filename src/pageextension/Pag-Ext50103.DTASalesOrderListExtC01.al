pageextension 50103 "DTASalesOrderListExtC01" extends "Sales Order List"
{
    actions
    {
        addlast(processing)
        {
            action(DTAExportarPedidosC01)
            {
                ApplicationArea = All;
                Caption = 'Exportar Pedidos';
                Image = Export;

                trigger OnAction()
                var

                begin
                    ExportarPedidosF();
                end;
            }
            action(DTAImportarPedidosC01)
            {
                ApplicationArea = All;
                Caption = 'Importar Pedidos';
                Image = Import;

                trigger OnAction()

                begin
                    ImportarPedidosF();
                end;
            }


        }
    }

    local procedure ExportarPedidosF()
    var
        rlSalesHeader: Record "Sales Header";
        TempLDTAConfiguracionC01: Record DTAConfiguracionC01 temporary;
        rlSalesLine: record "Sales Line";
        xlInStr: InStream;
        clSeparador: Label '·';
        xlOutStr: OutStream;
        xlLinea: Text;
        xlNombreFichero: Text;
    begin

        // obtenemos el dataset con los registros seleccionado por el usuario hay que crear pagina como variable
        CurrPage.SetSelectionFilter(rlSalesHeader);// traemos a modo de filtro lo seleccionado por el cliente en la pagina

        rlSalesHeader.SetLoadFields("Document Type", "No.", "Sell-to Customer No.", "Sell-to Customer Name", "External Document No.", "Location Code", "Posting Date", Status);
        if rlSalesHeader.FindSet(false) then begin

            TempLDTAConfiguracionC01.MiBlob.CreateOutStream(xloutStr, TextEncoding::Windows);
            repeat
                //recorremos el registro de cabeceras
                xlOutStr.WriteText('C');
                xlOutStr.WriteText(clSeparador);
                xlOutStr.WriteText(format(rlSalesHeader."Document Type"));
                xlOutStr.WriteText(clSeparador);
                xlOutStr.WriteText(rlSalesHeader."No.");
                xlOutStr.WriteText(clSeparador);
                xlOutStr.WriteText(rlSalesHeader."Sell-to Customer No.");
                xlOutStr.WriteText(clSeparador);
                xlOutStr.WriteText(rlSalesHeader."Sell-to Customer Name");
                xlOutStr.WriteText(clSeparador);
                xlOutStr.WriteText(rlSalesHeader."External Document No.");
                xlOutStr.WriteText(clSeparador);
                xlOutStr.WriteText(rlSalesHeader."Location Code");
                xlOutStr.WriteText(clSeparador);
                xlOutStr.WriteText(format(rlSalesHeader."Posting Date"));
                xlOutStr.WriteText(clSeparador);
                xlOutStr.WriteText(format(rlSalesHeader.Status));
                xlOutStr.WriteText();
                // recorremos las lineas
                rlSalesLine.SetRange("Document No.", rlSalesHeader."No.");
                rlSalesLine.SetRange("Document Type", rlSalesHeader."Document Type"); // primero tenemos que poner el filtro

                rlSalesLine.SetLoadFields("Document No.", "Document Type", "Line No.", Type, "No.", "Location Code", Quantity, Amount, "Unit Price");
                if rlSalesLine.FindSet(false) then begin
                    repeat
                        xlOutStr.WriteText('L');
                        xlOutStr.WriteText(clSeparador);
                        xlOutStr.WriteText(format(rlSalesLine."Document Type"));
                        xlOutStr.WriteText(clSeparador);
                        xlOutStr.WriteText(rlSalesLine."Document No.");
                        xlOutStr.WriteText(clSeparador);
                        xlOutStr.WriteText(format(rlSalesLine."Line No."));
                        xlOutStr.WriteText(clSeparador);
                        xlOutStr.WriteText(format(rlSalesLine.Type));
                        xlOutStr.WriteText(clSeparador);
                        xlOutStr.WriteText(rlSalesLine."No.");
                        xlOutStr.WriteText(clSeparador);
                        xlOutStr.WriteText(rlSalesLine."Location Code");
                        xlOutStr.WriteText(clSeparador);
                        xlOutStr.WriteText(format(rlSalesLine.Quantity));
                        xlOutStr.WriteText(clSeparador);
                        xlOutStr.WriteText(format(rlSalesLine."Unit Price"));
                        xlOutStr.WriteText(clSeparador);
                        xlOutStr.WriteText(format(rlSalesLine.Amount));
                        xlOutStr.WriteText();

                    until rlSalesLine.Next() = 0;
                end;

            until rlSalesHeader.Next() = 0;
            TempLDTAConfiguracionC01.MiBlob.CreateInStream(xlInStr);
            xlInStr.ReadText(xlLinea);
            // descargamos fichero
            xlNombreFichero := 'Pedidos.csv';
            if not DownloadFromStream(xlInStr, '', '', '', xlNombreFichero) then begin
                Error('No se ha podido descargar el fichero');
            end;

        end;

    end;

    local procedure ImportarPedidosF()
    var
        TempLDTAConfiguracionC01: Record DTAConfiguracionC01 temporary;
        rlSalesHeader: Record "Sales Header";
        rlSalesLine: Record "Sales Line";
        culDTACapturarErroresC01: Codeunit DTACapturarErroresC01;
        //xlFEcha: Date;
        //xlEstado: Enum "Sales Document Status";
        xlTipoDocumento: Enum "Sales Document Type";
        //xlTipoLinea: Enum "Sales Line Type";
        xlInStr: Instream;
        xlInt: Integer;
        clSeparador: label '·', Locked = true;
        //clNew: Label 'PE', Locked = true;
        xlLinea: Text;
    //DTAVblesGlobalesAppC01: Codeunit DTAVblesGlobalesAppC01;
    begin

        TempLDTAConfiguracionC01.MiBlob.CreateInStream(xlInStr, TextEncoding::Windows);
        if UploadIntoStream('', xlInStr) then begin
            while not xlInStr.EOS do begin //preguntamos si estamos al final del STREAM
                xlInStr.ReadText(xlLinea);

                //insertar cabecera o lineas en funcion del primer caracter de la linea
                case true of
                    xlLinea[1] = 'C':
                        //alta cabecera
                        begin
                            rlSalesHeader.Init();
                            if Evaluate(xlTipoDocumento, xlLinea.Split(clSeparador).Get(2)) then begin
                                rlSalesHeader.Validate("Document Type", xlTipoDocumento);
                            end;
                            rlSalesHeader.Validate("No.", xlLinea.Split(clSeparador).Get(3));
                            rlSalesHeader.Insert(true);
                            // en el mismo orden que exportamos
                            //rlSalesHeader.Validate("Sell-to Customer No.", xlLinea.Split(clSeparador).Get(4));
                            //rlSalesHeader.Validate("Sell-to Customer Name", xlLinea.Split(clSeparador).Get(5));
                            //rlSalesHeader.Validate("External Document No.", xlLinea.Split(clSeparador).Get(6));
                            //rlSalesHeader.Validate("Location Code", xlLinea.Split(clSeparador).Get(7));
                            // if Evaluate(xlFEcha, xlLinea.Split(clSeparador).Get(8)) then begin
                            //     rlSalesHeader.Validate("Posting Date", xlFEcha);
                            // end;
                            // if Evaluate(xlEstado, xlLinea.Split(clSeparador).Get(9)) then begin
                            //     rlSalesHeader.Validate(Status, xlEstado);
                            // end;
                            if not culDTACapturarErroresC01.AsignaCabeceraPedidoF(rlSalesHeader, 4, clSeparador, xlLinea) then begin
                                //capturar error y pasar a pagina de logs
                                GuardaMensajeErrorF(GetLastErrorText(), 'Codigo Cliente');
                                //Message('Error al asignar datos (%1', GetLastErrorText());
                            end else begin
                                rlSalesHeader.Modify(true);
                            end;
                            if not culDTACapturarErroresC01.AsignaCabeceraPedidoF(rlSalesHeader, 5, clSeparador, xlLinea) then begin
                                GuardaMensajeErrorF(GetLastErrorText(), 'Nombre Cliente');
                            end else begin
                                rlSalesHeader.Modify(true);
                            end;
                            if not culDTACapturarErroresC01.AsignaCabeceraPedidoF(rlSalesHeader, 6, clSeparador, xlLinea) then begin
                                GuardaMensajeErrorF(GetLastErrorText(), 'Nº documento externo');
                            end else begin
                                rlSalesHeader.Modify(true);
                            end;
                            if not culDTACapturarErroresC01.AsignaCabeceraPedidoF(rlSalesHeader, 7, clSeparador, xlLinea) then begin
                                GuardaMensajeErrorF(GetLastErrorText(), 'codigo almacen');
                            end else begin
                                rlSalesHeader.Modify(true);
                            end;
                            if not culDTACapturarErroresC01.AsignaCabeceraPedidoF(rlSalesHeader, 8, clSeparador, xlLinea) then begin
                                GuardaMensajeErrorF(GetLastErrorText(), 'Fecha de creacion');
                            end else begin
                                rlSalesHeader.Modify(true);
                            end;
                            if not culDTACapturarErroresC01.AsignaCabeceraPedidoF(rlSalesHeader, 9, clSeparador, xlLinea) then begin
                                GuardaMensajeErrorF(GetLastErrorText(), 'Estado');
                            end else begin
                                rlSalesHeader.Modify(true);
                            end;
                            //rlSalesHeader.Modify(true);
                        end;
                    xlLinea[1] = 'L':
                        //alta linea
                        begin
                            rlSalesLine.Init();
                            if Evaluate(xlTipoDocumento, xlLinea.Split(clSeparador).Get(2)) then begin
                                rlSalesLine.Validate("Document Type", xlTipoDocumento);
                            end;
                            rlSalesLine.Validate("Document No.", xlLinea.Split(clSeparador).Get(3));
                            if Evaluate(xlInt, xlLinea.Split(clSeparador).Get(4)) then begin
                                rlSalesLine.Validate("Line No.", xlInt);
                            end;
                            rlSalesLine.Insert(true);

                            // if Evaluate(xlTipoLinea, xlLinea.Split(clSeparador).Get(5)) then begin
                            //     rlSalesLine.Validate(Type, xlTipoLinea);
                            // end;
                            // rlSalesLine.Validate("No.", xlLinea.Split(clSeparador).Get(6));
                            // rlSalesLine.Validate("Location Code", xlLinea.Split(clSeparador).Get(7));
                            // if evaluate(xlInt, xlLinea.Split(clSeparador).Get(8)) then begin
                            //     rlSalesLine.Validate(Quantity, xlInt);
                            // end;
                            // if evaluate(xlInt, xlLinea.Split(clSeparador).Get(9)) then begin
                            //     rlSalesLine.Validate("Unit Price", xlInt);
                            // end;
                            // if evaluate(xlInt, xlLinea.Split(clSeparador).Get(10)) then begin
                            //     rlSalesLine.Validate(amount, xlInt);
                            // end;
                            if not culDTACapturarErroresC01.AsignaLineasPedidoF(rlSalesLine, 5, clSeparador, xlLinea) then begin
                                //capturar error y pasar a pagina de logs
                                GuardaMensajeErrorF(GetLastErrorText(), 'Tipo de dato');
                            end else begin
                                rlSalesLine.Modify(true);
                            end;
                            if not culDTACapturarErroresC01.AsignaLineasPedidoF(rlSalesLine, 6, clSeparador, xlLinea) then begin
                                GuardaMensajeErrorF(GetLastErrorText(), 'codigo articulo');
                            end else begin
                                rlSalesLine.Modify(true);
                            end;
                            if not culDTACapturarErroresC01.AsignaLineasPedidoF(rlSalesLine, 7, clSeparador, xlLinea) then begin
                                GuardaMensajeErrorF(GetLastErrorText(), 'Codigo almacen');
                            end else begin
                                rlSalesLine.Modify(true);
                            end;
                            if not culDTACapturarErroresC01.AsignaLineasPedidoF(rlSalesLine, 8, clSeparador, xlLinea) then begin
                                GuardaMensajeErrorF(GetLastErrorText(), 'Cantidad');
                            end else begin
                                rlSalesLine.Modify(true);
                            end;
                            if not culDTACapturarErroresC01.AsignaLineasPedidoF(rlSalesLine, 9, clSeparador, xlLinea) then begin
                                GuardaMensajeErrorF(GetLastErrorText(), 'Precio unidad');
                            end else begin
                                rlSalesLine.Modify(true);
                            end;
                            if not culDTACapturarErroresC01.AsignaLineasPedidoF(rlSalesLine, 10, clSeparador, xlLinea) then begin
                                GuardaMensajeErrorF(GetLastErrorText(), 'Importe');
                            end else begin
                                rlSalesLine.Modify(true);
                            end;
                            //rlSalesLine.Modify(true);
                        end;
                    else
                        Error('Tipo de linea %1 no reconocido', xlLinea[1]);
                end;
            end;
        end else begin
            Error('no se a podido importar el fichero');
        end;
    end;

    local procedure GuardaMensajeErrorF(pMensaje: Text; pCampo: Text)
    var
        // crear la tabla de log como temporal para no grabar muchos registros y hay una opciond e exportar a excel en BC si el usuario quiere las incidencias guardadas
        rlLogs: Record DTALogC01;
        xlTextoError: Text;
    begin
        rlLogs.Init();
        rlLogs.Validate(Id, CreateGuid());
        rlLogs.Insert(true); // aun siendo temporal seguiria siendo true para que asigne el id unico
        xlTextoError := 'Error al insertar campo:' + pCampo + ' ' + pMensaje;
        rlLogs.Validate(Mensaje, xlTextoError);
        rlLogs.Modify(true); // aqui siendo temporal seria false
    end;


}

