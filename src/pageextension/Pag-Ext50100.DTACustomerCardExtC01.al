pageextension 50100 "DTACustomerCardExtC01" extends "Customer Card"
{
    layout
    {
        addlast(content)
        {
            group(DTAVacunasCo1C01)
            {
                Caption = 'vacunas';

                field(DTANumVacunasC01; Rec.DTANumVacunasC01)
                {
                    ToolTip = ' Nº de Vacunas que ha recibido el cliente';
                    ApplicationArea = All;
                }
                field(DTAFechaUltimaVacunaC01; Rec.DTAFechaUltimaVacunaC01)
                {
                    ToolTip = 'Fecha en la que recibió la última Vacuna el cliente';
                    ApplicationArea = All;
                }
                field(DTATipoVacunaC01; Rec.DTATipoVacunaC01)
                {
                    ToolTip = 'Tipo de Vacuna que ha recibido el cliente';
                    ApplicationArea = All;
                }
            }
        }
    }
    actions
    {
        addlast(processing)
        {
            action(DTAAsignarVacunaBloqueadaC01)
            {
                ApplicationArea = All;
                Caption = '💉 Asignar vacuna bloqueada';

                trigger OnAction()
                begin
                    rec.Validate(DTATipoVacunaC01, 'BLOQUEADO');
                end;
            }
            action(DTASwitchVariableC01)
            {
                ApplicationArea = all;
                Caption = 'Prueba de variables globales a BC';
                trigger OnAction()
                var


                    clMensaje: Label 'El valor es %1. Lo cambiamos a %2';
                begin
                    Message(clMensaje, cuDTAFuncionesAppC01.GetSwitchF(), not cuDTAFuncionesAppC01.GetSwitchF()); // %1 significa que luego a la funcion puedo ponerle 1 parametro %2 - 2 parametros
                    //Message('el valor actual es ' + Format(xSwitch)+ 'lo cambiamos a '+Format(not xSwitch)); ES lo mismo que lo dearriba
                    cuDTAFuncionesAppC01.SetSwitchF(not cuDTAFuncionesAppC01.GetSwitchF());
                end;
            }
            action(DTACreaProveedorC01)
            {
                ApplicationArea = All;
                Caption = 'Crear Proveedor';
                Image = Vendor;
                Promoted = true; // el scope necesita que promoted sea true
                Scope = Repeater; // para que aparezca boton en las opciones de cada registro en los 3 puntos

                trigger OnAction()
                var
                    rlVendor: record Vendor;
                    xlbool: Boolean;
                begin
                    if rlVendor.get(Rec."No.") then begin
                        xlBool := Confirm('El proveedor ya exite. ¿Desea modificar sus datos?', false, rlVendor."No.");
                        page.Run(page::"Vendor Card", rlVendor);
                    end else begin
                        rlVendor.Init();
                        rlVendor.TransferFields(Rec, true, true);
                        rlVendor.Insert(true);
                    end;
                end;
            }
        }
    }
    var
        cuDTAFuncionesAppC01: Codeunit "DTAVblesGlobalesAppC01";

    trigger OnQueryClosePage(CloseAction: Action): Boolean
    begin
        exit(Confirm('¿Desea salir de esta página?', false));
    end;
}
