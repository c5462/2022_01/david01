pageextension 50101 "DTAVendorCardExtC01" extends "Vendor Card"
{
    layout
    {
    }
    actions
    {
        addlast(processing)
        {
            action(DTACrearClienteC01)
            {
                ApplicationArea = All;
                Caption = 'Crear Cliente';
                Image = Customer;

                trigger OnAction()
                var
                    rlCustomer: record Customer;
                    xlBool: Boolean;
                begin
                    if rlCustomer.get(rec."No.") then begin
                        xlBool := Confirm('El cliente ya existe. ¿Desea modificarlo?', false, rlCustomer."No.");
                        page.Run(page::"Customer Card", rlcustomer);
                    end else begin
                        rlCustomer.Init();
                        rlCustomer.TransferFields(Rec, true, true);
                        rlCustomer.Insert(true);
                    end;

                end;
            }
        }
    }

}
