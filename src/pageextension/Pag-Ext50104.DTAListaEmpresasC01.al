pageextension 50104 "DTAListaEmpresasC01" extends "Companies"
{
    layout
    {
        addlast(Control1)
        {
            field(DTANumClientesC01; NumRegistrosF(rlCustomer, rec.Name))
            {
                ApplicationArea = all;
                Caption = 'Nº clientes';
                trigger OnDrillDown()
                begin
                    //rec.Get(rec.Name);
                    rlCustomer.ChangeCompany(rec.Name);
                    Message('empresa: %1', rec.Name);
                    page.Run(0, rlCustomer);
                end;

            }
            field(DTANumProveedoresC01; NumRegistrosF(rlVendor, rec.Name))
            {
                ApplicationArea = all;
                Caption = 'Nº Proveedores';
                trigger OnDrillDown()
                begin
                    rec.Get(rec.Name);
                    rlVendor.ChangeCompany(rec.Name);
                    Message('empresa: %1', rec.Name);
                    page.Run(0, rlVendor);
                end;
            }
            field(DTANumCuentasC01; NumRegistrosF(rlCuentas, rec.Name))
            {
                ApplicationArea = all;
                Caption = 'Nº Cuentas';
                trigger OnDrillDown()
                begin
                    rec.Get(rec.Name);
                    rlCuentas.ChangeCompany(rec.Name);
                    Message('empresa: %1', rec.Name);
                    page.Run(0, rlCuentas);
                end;
            }
            field(DTANumProductosC01; NumRegistrosF(rlProductos, rec.Name))
            {
                ApplicationArea = all;
                Caption = 'Nº Productos';
                trigger OnDrillDown()
                begin
                    rec.Get(rec.Name);
                    rlProductos.ChangeCompany(rec.Name);
                    Message('empresa: %1', rec.Name);
                    page.Run(0, rlProductos);
                end;
            }

        }

    }
    actions
    {
        addlast(processing)
        {
            action(DTACopiaSeguridadC01)
            {
                ApplicationArea = All;
                Caption = 'Copia Seguridad';

                trigger OnAction()
                begin
                    CopiaSeguridadF();
                end;
            }
            action(DTARestaurarCopiaC01)
            {
                ApplicationArea = All;
                Caption = 'Restaurar Copia Seguridad';

                trigger OnAction()
                begin
                    RestaurarCopiaF();
                end;
            }
        }
    }



    local procedure NumRegistrosF(prClientes: Record Customer; pNombreEmpresa: Text) xSalida: Integer
    var

    begin
        prClientes.ChangeCompany(pNombreEmpresa);
        xSalida := prClientes.Count;
    end;

    local procedure NumRegistrosF(prProveedores: Record Vendor; pNombreEmpresa: Text) xSalida: Integer
    begin
        prProveedores.ChangeCompany(pNombreEmpresa);
        xSalida := prProveedores.Count;
    end;

    local procedure NumRegistrosF(prCuentas: Record "G/L Account"; pNombreEmpresa: Text) xSalida: Integer
    begin
        prCuentas.ChangeCompany(pNombreEmpresa);
        xSalida := prCuentas.Count;
    end;

    local procedure NumRegistrosF(prproductos: Record Item; pNombreEmpresa: Text) xSAlida: Integer
    begin
        prproductos.ChangeCompany(pNombreEmpresa);
        xSalida := prproductos.Count;
    end;

    local procedure CopiaSeguridadF()
    var
        rlAllObjWithCaption: Record AllObjWithCaption;
        rlCompany: Record Company;
        TempLDTAConfiguracionC01: Record DTAConfiguracionC01 temporary;
        culTypeHelper: Codeunit "Type Helper";
        xlRecRef: RecordRef;

        i: Integer;
        clSeparador: Label '·';
        xlOutStr: OutStream;
        xlInStr: InStream;
        xlTextBuilder: TextBuilder;
        xlFichero: Text;
    begin
        //TempLDTAConfiguracionC01.MiBlob.CreateOutStream(xlOutStr,TextEncoding::Windows);
        CurrPage.SetSelectionFilter(rlCompany); // para que haga copia solo de las seleccionadas por el cliente
        if rlCompany.FindSet(false) then begin
            repeat
                // escribir en el fichero
                //inicio empresa
                xlTextBuilder.Append('IE<' + rlCompany.Name + '>' + culTypeHelper.CRLFSeparator());
                // bucle de tablas
                rlAllObjWithCaption.SetRange("Object Type", rlAllObjWithCaption."Object Type"::Table);
                rlAllObjWithCaption.SetFilter("Object ID", '%1|%2|%3|%4', Database::"G/L Account", Database::Customer, Database::Vendor, Database::Item);
                if rlAllObjWithCaption.FindSet(false) then begin
                    repeat
                        //inicio tabla
                        xlTextBuilder.Append('IT<' + rlAllObjWithCaption."Object Name" + '>' + culTypeHelper.CRLFSeparator());
                        //bucle de registros
                        xlRecRef.Open(rlAllObjWithCaption."Object ID", false, rlCompany.Name);
                        if xlRecRef.FindSet(false) then begin
                            repeat
                                //inicio registros
                                xlTextBuilder.Append('IR<' + format(xlRecRef.RecordId) + '>' + culTypeHelper.CRLFSeparator());
                                //bucle de campos
                                for i := 1 to xlRecRef.FieldCount do begin
                                    // para que saque campos calculados y no campos de filtro
                                    if xlRecRef.FieldIndex(i).Class <> xlRecRef.FieldIndex(i).Class::FlowFilter then begin
                                        if xlRecRef.FieldIndex(i).Class = xlRecRef.FieldIndex(i).Class::FlowField then begin
                                            xlRecRef.FieldIndex(i).CalcField();
                                        end;
                                        if format(xlRecRef.FieldIndex(i).Value) <> '' then begin
                                            xlTextBuilder.Append('<');
                                            xlTextBuilder.Append(xlRecRef.FieldIndex(i).Name);
                                            xlTextBuilder.Append('>');
                                            xlTextBuilder.Append(clSeparador);
                                            xlTextBuilder.Append('<');
                                            xlTextBuilder.Append(xlRecRef.FieldIndex(i).Value);
                                            xlTextBuilder.Append('>');
                                            xlTextBuilder.Append(culTypeHelper.CRLFSeparator());
                                        end;
                                    end;
                                end;
                                //fin registros
                                xlTextBuilder.Append('FR<' + format(xlRecRef.RecordId) + '>' + culTypeHelper.CRLFSeparator());
                            until xlRecRef.Next() = 0;
                        end;

                        //cerramos recordref
                        xlRecRef.Close();
                        //fin tabla
                        xlTextBuilder.Append('FT<' + rlAllObjWithCaption."Object Name" + '>' + culTypeHelper.CRLFSeparator());
                    until rlAllObjWithCaption.Next() = 0;
                end;
                //fin empresa
                xlTextBuilder.Append('FE<' + rlCompany.Name + '>' + culTypeHelper.CRLFSeparator());
            until rlCompany.Next() = 0;
            // escribimos en un contenedor
            TempLDTAConfiguracionC01.MiBlob.CreateOutStream(xlOutStr, TextEncoding::Windows);
            xlOutStr.WriteText(xlTextBuilder.ToText());
            // descargar fichero
            TempLDTAConfiguracionC01.MiBlob.CreateInStream(xlInStr, TextEncoding::Windows);
            xlFichero := 'copiaSeguridad.csv';
            DownloadFromStream(xlInStr, '', '', '', xlFichero);

        end;
    end;

    local procedure ValorCampoF(pfieldref: FieldRef; pValor: Text): Text
    var

    begin
        if pfieldref.type = pfieldref.Type::Option then begin

        end else begin
            exit(pValor);
        end;
    end;

    local procedure RestaurarCopiaF()
    var

        TempLDTAConfiguracionC01: Record DTAConfiguracionC01 temporary;
        xlRecRef: RecordRef;
        xlInStr: Instream;
        xlLinea: Text;
    begin
        // subir fichero de copias de seguridad
        TempLDTAConfiguracionC01.MiBlob.CreateInStream(xlInStr, TextEncoding::Windows);
        if UploadIntoStream('', xlInStr) then begin
            // recorrer el fichero
            while not xlInStr.EOS do begin
                xlInStr.ReadText(xlLinea);
                case true of

                    //si es inicio de tabla abrir el registro
                    //xlLinea.Contains('IT<'):
                    copystr(xlLinea, 1, 3) = 'IT<':
                        begin
                            xlRecRef.open(DameNumTablaF(CopyStr(xlLinea, 4).TrimEnd('>')));

                        end;
                    //xlLinea.Contains('IR<'):
                    // si es inicio de registro crearemos el registro
                    copystr(xlLinea, 1, 3) = 'IR<':
                        begin
                            //inicializamos el registro
                            xlRecRef.Init();
                        end;
                    //por cada campo asignar su valor
                    xlLinea.StartsWith('<'):
                        begin
                            //comprobamos que no sea un campo calculado
                            if xlRecRef.Field(NumCampoF(xlRecRef.Number, xlLinea.Split('·').Get(1).TrimStart('<').TrimEnd('>'))).Class = FieldClass::Normal then begin

                                xlRecRef.Field(NumCampoF(xlRecRef.Number, xlLinea.Split('·').Get(1).TrimStart('<').TrimEnd('>'))).Value :=
                                CampoYValorF(xlRecRef.Field(NumCampoF(xlRecRef.Number, xlLinea.Split('·').Get(1).TrimStart('<').TrimEnd('>'))), xlLinea.Split('·').Get(2).TrimStart('<').TrimEnd('>'));
                                // no hacemos aqui validate porque no queremos que haga mas cosas

                            end;
                        end;
                    //si es fin de registro insertar registro
                    copystr(xlLinea, 1, 3) = 'FR<':
                        begin
                            xlRecRef.Insert(false); // aqui es false porque solo queremos importar lo que venga en fichero
                        end;
                    //si es fin de tabla cerrar el registro
                    copystr(xlLinea, 1, 3) = 'FT<':
                        begin
                            xlRecRef.Close();
                        end;
                    xlLinea.Contains('·'):
                        begin

                        end;
                end;
            end;
        end else begin
            Error('no se ha podido subir fichero al servidor. error %1', GetLastErrorText());
        end;
    end;

    local procedure DameNumTablaF(pNombreTabla: Text): Integer
    var
        // se puede hacer tb con un record de allObjectwithcaption con 2 setrange en vez de uno
        rlField: Record field;

    begin
        rlField.SetRange(TableName, pNombreTabla);
        rlField.SetLoadFields(TableNo);
        // aqui no utilizar findset porque no queremos todos los registros,solo uno,tarda menos en SQL
        if rlField.FindFirst() then begin
            exit(rlField.TableNo);
        end;
    end;

    local procedure NumCampoF(pNumTabla: Integer; pNombreCampo: Text): Integer
    var
        rlField: Record field;
    begin
        rlfield.SetRange(TableNo, pNumTabla);
        rlfield.SetRange(FieldName, pNombreCampo);
        rlField.SetLoadFields("No.");
        if rlfield.FindFirst() then begin
            exit(rlField."No.");
        end;
    end;

    local procedure CampoYValorF(pFieldRef: FieldRef; pValor: Text): Variant
    var
        rlField: Record field;
        xlBigInteger: BigInteger;
        i: Integer;
        xlBooleano: Boolean;
        xlDecimal: Decimal;
        xlFecha: Date;
        TempLDTAConfiguracionC01: Record DTAConfiguracionC01 temporary;
        TempLItem: Record Item temporary;
        TempLCustomer: Record Customer temporary;
        xlGuid: Guid;
        xldatetime: DateTime;
        xltime: Time;
    begin
        case pFieldRef.Type of
            pfieldRef.Type::Code, pfieldref.Type::text:
                begin

                    exit(pValor);
                end;
            pFieldRef.Type::biginteger, pfieldref.Type::Integer:
                begin

                    if Evaluate(xlBigInteger, pValor) then begin
                        exit(xlBigInteger);
                    end;
                end;
            pFieldRef.type::Option:
                begin
                    for i := 1 to pFieldRef.EnumValueCount() do begin
                        if pFieldRef.GetEnumValueCaption(i).ToLower() = pValor.ToLower() then begin
                            exit(i);
                        end;
                    end;
                end;
            pFieldRef.type::Boolean:
                begin

                    if Evaluate(xlBooleano, pValor) then begin
                        exit(xlBooleano);
                    end;
                end;
            pFieldRef.type::Decimal:
                begin

                    if Evaluate(xlDecimal, pValor) then begin
                        exit(xlDecimal);
                    end;
                end;
            pFieldRef.type::Date:
                begin

                    if Evaluate(xlFecha, pValor) then begin
                        exit(xlFecha);
                    end;
                end;
            pFieldRef.Type::Blob:
                exit(TempLDTAConfiguracionC01.MiBlob);
            pFieldRef.Type::Guid:
                begin

                    if Evaluate(xlGuid, pValor) then begin
                        exit(xlGuid);
                    end;
                end;
            pFieldRef.Type::DateTime:
                begin

                    if Evaluate(xldatetime, pValor) then begin
                        exit(xldatetime);
                    end;
                end;
            pFieldRef.Type::Time:
                begin

                    if Evaluate(xltime, pValor) then begin
                        exit(xltime);
                    end;
                end;
            pFieldRef.Type::Media:
                begin

                    exit(TempLCustomer.Image);
                end;
            pFieldRef.Type::MediaSet:
                begin

                    exit(TempLItem.picture);
                end;

            else
                exit(pValor);

        end;
    end;



    var
        rlCustomer: Record Customer;
        rlVendor: Record Vendor;
        rlCuentas: Record "G/L Account";
        rlProductos: Record Item;

}
