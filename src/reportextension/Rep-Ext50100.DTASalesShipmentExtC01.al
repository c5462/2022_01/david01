/// <summary>
/// Extension de report sales shipment 
/// </summary>
reportextension 50100 "DTASalesShipmentExtC01" extends "Sales - Shipment"
{
    dataset
    {
        addlast("Sales Shipment Header")
        {

            dataitem("DTASalesShipmentHeaderC01"; "Sales Shipment Header")
            {

                column(DTAShiptoName_DTASalesShipmentHeaderC01; "Ship-to Name")
                {
                }
                column(DTAShiptoCity_DTASalesShipmentHeaderC01; "Ship-to City")
                {
                }
                column(DTAShiptoAddress_DTASalesShipmentHeaderC01; "Ship-to Address")
                {
                }
            }
        }

    }
}