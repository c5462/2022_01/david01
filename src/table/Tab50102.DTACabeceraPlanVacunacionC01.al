table 50102 "DTACabeceraPlanVacunacionC01"
{
    Caption = 'Cab. Plan Vacunacion';
    DataClassification = ToBeClassified;
    LookupPageId = DTAListaCabPlanVacunacionC01;

    fields
    {
        field(1; CodigoCabecera; Code[20])
        {
            Caption = 'Código';
            DataClassification = CustomerContent;
        }
        field(2; Descripcion; Text[50])
        {
            Caption = 'Descripción';
            DataClassification = CustomerContent;
        }
        field(3; FechaInicioVacunacion; Date)
        {
            Caption = 'Fecha Inicio Vacunación';
            DataClassification = CustomerContent;
        }
        field(4; EmpresaVacunadora; Code[20])
        {
            Caption = 'Empresa Vacunadora';
            DataClassification = CustomerContent;
            TableRelation = vendor."No.";
        }
        field(5; FechaProximaVacunacion; Date)
        {
            Caption = 'Fecha Proxima Vacunacion';
            DataClassification = SystemMetadata;
        }
    }
    keys
    {
        key(PK; CodigoCabecera)
        {
            Clustered = true;
        }
    }


    procedure NombreEmpresaVacunadoraF(): Text
    var
        rlVendor: Record Vendor;
    begin

        if rlVendor.Get(rec.EmpresaVacunadora) then begin
            exit(rlVendor.Name);
        end;
    end;

    trigger OnDelete()
    var
        rlDTALineasPlanVacunacionC01: record DTALineasPlanVacunacionC01;
    begin
        rlDTAlineasPlanVacunacionC01.SetRange(CodigoLineas, Rec.CodigoCabecera);
        rlDTALineasPlanVacunacionC01.DeleteAll(true); // si es tabla temporal poner false

        // if rlDTAlineasPlanVacunacionC01.FindSet(true, true) then begin
        //     repeat
        //         if rlDTALineasPlanVacunacionC01.Delete(true) then begin

        //         end else begin
        //             Error('no ha podido borrar la linea');
        //         end;
        //     until rlDTALineasPlanVacunacionC01.Next() = 0;
        // end;
    end;
}
