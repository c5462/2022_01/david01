table 50101 "DTAVariosC01"
{
    Caption = 'VariosC01';
    DataClassification = SystemMetadata;
    LookupPageId = DTAListadeVariosC01;
    DrillDownPageId = DTAlistadeVariosC01;

    fields
    {
        field(1; Tipo; Enum DTATipoC01)
        {
            Caption = 'Tipo';
            DataClassification = SystemMetadata;
        }
        field(2; Codigo; Code[20])
        {
            Caption = 'Código';
            DataClassification = SystemMetadata;
        }
        field(3; Descripcion; Text[50])
        {
            Caption = 'Descripción';
            DataClassification = SystemMetadata;
        }
        field(4; Bloqueado; Boolean)
        {
            Caption = 'Bloqueado';
            DataClassification = SystemMetadata;
        }
        field(5; FechaSegundaDosis; DateFormula)
        {
            Caption = 'Fecha Segunda Dosis';
            DataClassification = SystemMetadata;
        }

    }
    keys
    {
        key(PK; Tipo, Codigo)
        {
            Clustered = true;
        }
    }
    fieldgroups
    {
        fieldgroup(DropDown; Codigo, Descripcion)
        {

        }
    }
    trigger OnDelete()
    begin
        rec.TestField(Bloqueado, true);
    end;
}
