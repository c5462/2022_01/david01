table 50103 "DTALineasPlanVacunacionC01"
{
    Caption = 'Líneas Plan Vacunación';
    DataClassification = ToBeClassified;
    //LookupPageId = DTASubPageLineasVacunacionC01;
    DrillDownPageId = DTASubPageLineasVacunacionC01;


    fields
    {
        field(1; CodigoLineas; Code[20])
        {
            Caption = 'Código';
            DataClassification = CustomerContent;
        }
        field(2; NumLinea; Integer)
        {
            Caption = 'Nº de Línea';
            DataClassification = CustomerContent;
        }
        field(3; ClienteAVacunar; Code[20])
        {
            Caption = 'Cliente a Vacunar';
            DataClassification = CustomerContent;
            TableRelation = Customer."No.";
            trigger OnValidate()
            var
                rlDTACabeceraPlanVacunacionC01: Record DTACabeceraPlanVacunacionC01;
            begin
                if Rec.FechaVacunacion = 0D then begin
                    if rlDTACabeceraPlanVacunacionC01.Get(rec.CodigoLineas) then begin
                        rec.Validate(FechaVacunacion, rlDTACabeceraPlanVacunacionC01.FechaInicioVacunacion);
                    end;
                end;
            end;
        }
        field(4; FechaVacunacion; Date)
        {
            Caption = 'Fecha de  Vacunación';
            DataClassification = CustomerContent;
            trigger OnValidate()

            begin
                CalcularFechaProxVacunacionF();

            end;
        }
        field(5; CodigoVacuna; code[20])
        {
            Caption = 'Tipo Vacuna';
            DataClassification = CustomerContent;
            TableRelation = DTAVariosC01.Codigo where(Tipo = const(vacuna), Bloqueado = const(false));
            trigger OnValidate()

            begin
                CalcularFechaProxVacunacionF();

            end;
        }
        field(6; CodigoOtros; Code[20])
        {
            Caption = 'Tipo otros';
            DataClassification = CustomerContent;
            TableRelation = DTAVariosC01.Codigo where(Tipo = const(otros), Bloqueado = const(false));
        }
        field(7; FechaProximaVacunacion; Date)
        {
            Caption = 'Fecha proxima vacunacion';
            DataClassification = SystemMetadata;
        }
    }
    keys
    {
        key(PK; CodigoLineas, NumLinea)
        {
            Clustered = true;
        }
    }


    procedure NombreclienteF(): Text
    var
        rlCustomer: Record Customer;
    begin
        if rlCustomer.Get(rec.ClienteAVacunar) then begin
            exit(rlCustomer.Name);
        end;
    end;
    /// <summary>
    /// Devuelve la descripción de la vacuna o de otros, en función del parámetro Tipo y del código pasado en el parámetro
    /// </summary>
    /// <param name="pTipo">enum DTATipoC01.</param>
    /// <param name="pCodVarios">code[20].</param>
    /// <returns>Return value of type Text.</returns>
    procedure DescripcionVariosF(pTipo: enum DTATipoC01; pCodVarios: code[20]): Text
    var
        rlvarios: Record DTAVariosC01;
    begin
        if rlVarios.get(pTipo, pCodVarios) then begin
            exit(rlVarios.Descripcion);
        end;
    end;

    local procedure CalcularFechaProxVacunacionF()
    var
        rlDTAVariosC01: Record DTAVariosC01;
        ishandled: Boolean;
    begin
        OnBeforeCalcularFechaProxVacunaF(Rec, xRec, rlDTAVariosC01, ishandled);
        if not ishandled then begin
            if rec.FechaVacunacion <> 0D then begin
                if rlDTAVariosC01.Get(rlDTAVariosC01.Tipo::vacuna, rec.CodigoVacuna) then begin
                    rec.Validate(FechaProximaVacunacion, CalcDate(rlDTAVariosC01.FechaSegundaDosis, rec.FechaVacunacion));
                end;
            end;
        end;
        OnAfterCalcularFechaProxVacunaF(Rec, xRec, rlDTAVariosC01);
    end;

    [IntegrationEvent(false, false)]
    local procedure OnBeforeCalcularFechaProxVacunaF(Rec: Record DTALineasPlanVacunacionC01; xRec: Record DTALineasPlanVacunacionC01; rlDTAVariosC01: Record DTAVariosC01; var ishandled: Boolean)
    begin
    end;

    [IntegrationEvent(false, false)]
    local procedure OnAfterCalcularFechaProxVacunaF(Rec: Record DTALineasPlanVacunacionC01; xRec: Record DTALineasPlanVacunacionC01; rlDTAVariosC01: Record DTAVariosC01)
    begin
    end;


}
