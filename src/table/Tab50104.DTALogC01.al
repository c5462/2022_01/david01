table 50104 "DTALogC01"
{
    Caption = 'Log';
    DataClassification = SystemMetadata;

    fields
    {
        field(1; Id; Guid)
        {
            Caption = 'Id';
            DataClassification = SystemMetadata;
        }
        field(2; Mensaje; Text[250])
        {
            Caption = 'Mensaje';
            DataClassification = SystemMetadata;
        }
    }
    keys
    {
        key(PK; Id)
        {
            Clustered = true;
        }
        key(PK2; SystemCreatedAt)
        {

        }
    }
    trigger OnInsert()

    begin
        if IsNullGuid(rec.Id) then begin
            rec.validate(rec.Id, CreateGuid());
        end;
    end;
}
