table 50100 "DTAConfiguracionC01"
{
    Caption = 'Configuración de la app';
    DataClassification = OrganizationIdentifiableInformation;


    fields
    {
        field(1; Id; Code[10])
        {
            Caption = 'Id. de registro';
            DataClassification = SystemMetadata;

        }
        field(2; CodCteWeb; Code[20])
        {
            Caption = 'Codigo cliente para web';
            DataClassification = SystemMetadata;
            TableRelation = Customer."No.";
        }
        field(3; TextoRegistro; Text[250])
        {
            Caption = 'Texto Registro';
            DataClassification = SystemMetadata;
        }
        field(4; TipoDoc; Enum DTATipoDocC01)
        {
            Caption = 'Tipo de documento';
            DataClassification = SystemMetadata;
        }
        field(5; NombreCliente; Text[100])
        {
            Caption = 'Nombre de Cliente';
            FieldClass = FlowField;
            CalcFormula = lookup(Customer.Name where("No." = field(CodCteWeb)));
            Editable = false;
        }

        field(6; TipoTabla; Enum DTATipoTablaC01)
        {
            Caption = 'Tipo de tabla';
            DataClassification = SystemMetadata;
            trigger OnValidate()
            begin
                if xRec.TipoTabla <> rec.TipoTabla then begin

                    rec.Validate(codTabla, '');
                end;
            end;

        }
        field(7; codTabla; Code[20])
        {
            Caption = 'Código de Tabla';
            DataClassification = SystemMetadata;
            TableRelation = if (TipoTabla = const(Cliente)) Customer else
            if (TipoTabla = const(Contacto)) Contact else
            if (TipoTabla = const(Empleado)) Employee else
            if (TipoTabla = const(Proveedor)) Vendor else
            if (TipoTabla = const(Recurso)) Resource;
        }
        field(8; ColorFondo; Enum DTAColoresC01)
        {
            Caption = 'color de fondo';
            DataClassification = SystemMetadata;

        }
        field(9; ColorLetra; Enum DTAColoresC01)
        {
            Caption = 'color de letra';
            DataClassification = SystemMetadata;

        }
        field(10; UnidadesDisponibles; Decimal)
        {
            Caption = 'Nº unidades disponibles';
            FieldClass = FlowField;
            CalcFormula = sum("Item Ledger Entry".Quantity where("Item No." = field(FiltroProducto), "Posting Date" = field(FiltroFechaRegistro), "Entry Type" = field(FiltroTipoMovimiento)));
        }
        field(11; FiltroProducto; code[20])
        {
            TableRelation = Item."No.";
            Caption = 'filtro producto';
            FieldClass = FlowFilter;
        }
        field(12; FiltroFechaRegistro; Date)
        {

            Caption = 'Filtro fecha';
            FieldClass = FlowFilter;
        }
        field(13; FiltroTipoMovimiento; Enum "Item Ledger Entry Type")
        {
            Caption = 'Filtro Tipo de movimiento';
            FieldClass = FlowFilter;

        }
        field(14; IdPassword; Guid)
        {
            DataClassification = SystemMetadata;
        }
        field(15; MiBlob; Blob)
        {
            DataClassification = SystemMetadata;
        }





    }
    keys
    {
        key(PK; Id)
        {
            Clustered = true;
        }
    }
    /// <summary>
    /// Recupera el registro de la tabla actual y si no existe lo crea.
    /// </summary>
    /// <returns>Return value of type Boolean.</returns>
    procedure GetF(): Boolean
    begin
        if not rec.get() then begin
            rec.Init();
            rec.Insert(true);
        end;
        exit(true)
    end;

    /// <summary>
    /// Devuelve el nombre del cliente pasado en el parámetro pcodCte. Si no existe devolverá un texto vacio
    /// </summary>
    /// <param name="pCodCte"></param>
    /// <returns></returns>
    procedure NombreClienteF(pCodCte: Code[20]): Text
    var
        rlCustomer: Record Customer;
    begin
        if rlCustomer.Get(pCodCte) then begin
            exit(rlCustomer.Name);
        end;
    end;

    procedure NombreTablaF(pTipoTabla: Enum DTATipoTablaC01; pCodTabla: Code[20]): Text
    var

        rlcontact: Record Contact;

        rlproveedor: Record Vendor;

        rlempleado: Record Employee;

        rlrecurso: Record Resource;
        rlDTAConfiguracionC01: Record DTAConfiguracionC01;
    begin
        case pTipoTabla of
            pTipoTabla::Cliente:
                begin
                    exit(rlDTAConfiguracionC01.NombreClienteF(pCodTabla));
                end;
            pTipoTabla::Contacto:
                begin
                    if rlContact.Get(pCodTabla) then begin
                        exit(rlcontact.Name);
                    end;
                end;

            pTipoTabla::Proveedor:
                begin
                    if rlproveedor.Get(pCodTabla) then begin
                        exit(rlproveedor.Name);
                    end;
                end;
            pTipoTabla::Empleado:
                begin
                    if rlempleado.Get(pCodTabla) then begin
                        exit(rlempleado.Name);
                    end;
                end;
            pTipoTabla::Recurso:
                begin
                    if rlrecurso.Get(pCodTabla) then begin
                        exit(rlrecurso.Name);
                    end;

                end;

        end;
    end;

    [NonDebuggable]
    procedure passwordF(pPassword: text)

    begin
        rec.Validate(IdPassword, CreateGuid());
        rec.Modify(true);
        IsolatedStorage.Set(rec.IdPassword, pPassword, DataScope::Company);
    end;

    [NonDebuggable]
    procedure passwordF() xSalida: Text

    begin
        if not IsolatedStorage.Get(rec.IdPassword, DataScope::Company, xSalida) then begin
            Error('No se encuentra el password');
        end;
    end;
}
