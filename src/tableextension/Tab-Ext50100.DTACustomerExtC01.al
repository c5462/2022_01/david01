tableextension 50100 "DTACustomerExtC01" extends Customer
{

    fields
    {
        field(50100; DTANumVacunasC01; Integer)
        {
            Caption = 'Nº de Vacunas';
            Editable = false;
            FieldClass = FlowField;
            CalcFormula = count(DTALineasPlanVacunacionC01 where(ClienteAVacunar = field("No.")));
        }
        field(50101; DTAFechaUltimaVacunaC01; Date)
        {
            Caption = 'Fecha última Vacuna';
            Editable = false;
            FieldClass = FlowField;
            CalcFormula = max(DTALineasPlanVacunacionC01.FechaVacunacion where(ClienteAVacunar = field("No.")));
        }
        field(50102; DTATipoVacunaC01; Code[20])
        {
            Caption = 'Tipo de Vacuna';
            DataClassification = CustomerContent;
            TableRelation = DTAVariosC01.Codigo where(Tipo = const(vacuna));
            trigger OnValidate()

            var
                rlDTAVariosC01: Record DTAVariosC01;
            begin
                if rlDTAVariosC01.Get(rlDTAVariosC01.Tipo::vacuna, rec.dtatipovacunaC01) then begin
                    rlDTAVariosC01.TestField(Bloqueado, false);
                end;
            end;
        }
        modify(Name)
        {
            trigger OnAfterValidate()
            var
                culDTAVblesGlobalesAppC01: Codeunit DTAVblesGlobalesAppC01;
            begin
                culDTAVblesGlobalesAppC01.NombreF(rec.Name);
            end;
        }

    }

    // recorremos la tabla de empresas para una vez que insertemos un cliente nos lo inserte tb en todas las empresas de la tabla empresas
    trigger OnAfterInsert()

    begin
        SincronizarRegistros(xAccion::insert);
    end;


    trigger OnAfterModify()
    begin
        SincronizarRegistros(xAccion::modify);
    end;

    trigger OnAfterDelete()
    begin
        SincronizarRegistros(xAccion::delete);
    end;



    local procedure SincronizarRegistros(pAction: Option)
    var
        rlCustomer: Record Customer;
        rlCompany: Record Company;
    begin
        //filtramos para que no coja la empresa actual
        rlCompany.SetFilter(name, '<>%1', CompanyName);
        //recorrer empresas
        if rlCompany.FindSet(false) then begin
            repeat
                //cambiar la variable local de registro de clientes
                rlCustomer.ChangeCompany(rlCompany.Name);
                //asignar valores de cliente a la variable local
                rlCustomer.Init();
                rlCustomer.Copy(rec);
                case paction of
                    xaccion::insert:
                        //insertar el cliente
                        rlCustomer.Insert(false); // aqui sin true para que no cree el contacto
                    xAccion::modify:
                        //modificar cliente
                        rlCustomer.Modify(false);
                    xAccion::delete:
                        //eliminar cliente
                        rlCustomer.Delete(false);

                end

            until rlCompany.Next() = 0;

        end;


    end;

    var
        xAccion: option insert,modify,delete;

}

