tableextension 50101 "DTAVendorExtC01" extends Vendor
{

    trigger OnAfterInsert()
    var
        cuDTAVblesGlobalesAppC01: Codeunit DTAVblesGlobalesAppC01;
    begin
        rec.Validate(rec.Name, cuDTAVblesGlobalesAppC01.NombreF());
    end;

}
